"""Tests for constructing class types."""

from dataclasses import dataclass
from functools import partial
from typing import Any, Callable, List, Mapping, NamedTuple, Tuple

import attr
import pytest

from terramare import (
    ARRAY,
    OBJECT,
    VALUE,
    auto,
    classes,
    disallow_unknown_fields,
    handle_exception_types,
    terramare,
)
from terramare.errors import ConstructorError, FactoryError, UnsupportedTargetTypeError
from terramare.metadata import MetadataCollection
from terramare.types import Primitive

from . import test_utils
from .test_utils import raises


@disallow_unknown_fields
@auto(from_=OBJECT | ARRAY)
def _no_param_fn() -> None:
    pass


@pytest.mark.parametrize(
    ["input_value", "expected_output_value"],
    [pytest.param({}, None, id="from object"), pytest.param([], None, id="from array")],
)
def test_construct_zero_parameter_function_call_mainline(
    input_value: Primitive, expected_output_value: Primitive
) -> None:
    """Test constructing a function call with no parameters."""
    assert terramare.structure(input_value, into=_no_param_fn) == expected_output_value


@pytest.mark.parametrize(
    ["input_value", "error_contains"],
    [
        pytest.param([0], ["1", "0"], id="from array, unexpected parameter"),
        pytest.param({"x": 1}, ["x"], id="from object, unexpected parameter"),
    ],
)
def test_construct_zero_parameter_function_call_error(
    input_value: Primitive, error_contains: List[str]
) -> None:
    """Test error cases when constructing a function call with no parameters."""
    with raises(ConstructorError, assert_contains=error_contains):
        terramare.structure(input_value, into=_no_param_fn)


@auto(from_=OBJECT | ARRAY | VALUE)
def _untyped_fn(x):  # type: ignore[no-untyped-def]
    return x


@pytest.mark.parametrize(
    ["input_value", "expected_output_value"],
    [
        pytest.param({"x": 1}, 1, id="from object"),
        pytest.param([1], 1, id="from array"),
        pytest.param(1, 1, id="from leaf"),
        pytest.param(
            {"x": {"key": "value"}}, {"key": "value"}, id="from nested object"
        ),
        pytest.param([{"key": "value"}], {"key": "value"}, id="from nested array"),
        pytest.param({"x": 1, "y": 2}, 1, id="extra parameter (object)"),
        pytest.param([1, 2], 1, id="extra parameter (array)"),
    ],
)
def test_construct_untyped_function_call_mainline(
    input_value: Primitive, expected_output_value: Primitive
) -> None:
    """Test constructing a function call with a single untyped parameter."""
    assert terramare.structure(input_value, into=_untyped_fn) == expected_output_value


@auto(from_=OBJECT | ARRAY | VALUE)
def _single_param_fn(int_: int) -> int:
    return int_


@pytest.mark.parametrize(
    ["input_value", "expected_output_value"],
    [
        pytest.param({"int_": 1}, 1, id="from object"),
        pytest.param([1], 1, id="from array"),
        pytest.param(1, 1, id="from leaf"),
    ],
)
def test_construct_single_parameter_function_call_mainline(
    input_value: Primitive, expected_output_value: Primitive
) -> None:
    """Test constructing a function call with a single typed parameter."""
    assert (
        terramare.structure(input_value, into=_single_param_fn) == expected_output_value
    )


@pytest.mark.parametrize(
    ["input_value", "error_contains"],
    [
        pytest.param({}, "missing required field", id="missing field"),
        pytest.param([], "too few", id="missing element"),
        pytest.param({"int_": "string"}, "expected integer", id="field type mismatch"),
        pytest.param(["string"], "expected integer", id="element type mismatch"),
        pytest.param("string", "expected integer", id="leaf type mismatch"),
    ],
)
def test_construct_single_parameter_function_call_error(
    input_value: Primitive, error_contains: str
) -> None:
    """Test error cases when constructing a function call with a single typed parameter."""
    with raises(ConstructorError, assert_contains=error_contains):
        terramare.structure(input_value, into=_single_param_fn)


@auto(from_=ARRAY)
def _string_type_param_fn(int_: "int") -> int:
    return int_


def test_construct_string_type_parameter_function_call_mainline() -> None:
    """Test constructing a function call with a single typed parameter."""
    assert terramare.structure([1], into=_string_type_param_fn) == 1


@pytest.mark.parametrize(
    ["input_value", "error_contains"],
    [
        pytest.param([], "too few", id="missing elements"),
        pytest.param({}, "expected array", id="type mismatch"),
    ],
)
def test_construct_string_type_parameter_function_call_error(
    input_value: Primitive, error_contains: str
) -> None:
    """Test error cases when constructing a function call with a single typed parameter."""
    with raises(ConstructorError, assert_contains=error_contains):
        terramare.structure(input_value, into=_string_type_param_fn)


@auto(from_=VALUE)
def _optional_param_fn(int_: int = 0) -> int:
    return int_


def test_construct_optional_parameter_function_call_mainline() -> None:
    """Test constructing a function call with a single optional parameter."""
    assert terramare.structure(1, into=_optional_param_fn) == 1


def test_construct_optional_parameter_function_call_error() -> None:
    """Test error cases when constructing a function call with a single optional parameter."""
    with raises(ConstructorError, assert_contains="expected integer"):
        terramare.structure("string", into=_optional_param_fn)


@auto(from_=OBJECT | ARRAY | VALUE)
def _param_fn(int_: int, str_: str = "") -> Primitive:
    return {"int_": int_, "str_": str_}


@pytest.mark.parametrize(
    ["input_value", "expected_output_value"],
    [
        pytest.param({"int_": 1}, {"int_": 1, "str_": ""}, id="no optional field"),
        pytest.param([1], {"int_": 1, "str_": ""}, id="no optional element"),
        pytest.param(1, {"int_": 1, "str_": ""}, id="leaf"),
        pytest.param(
            {"int_": 1, "str_": "string"},
            {"int_": 1, "str_": "string"},
            id="optional field",
        ),
        pytest.param(
            [1, "string"],
            {"int_": 1, "str_": "string"},
            id="optional element",
        ),
    ],
)
def test_construct_standard_function_call_mainline(
    input_value: Primitive, expected_output_value: Primitive
) -> None:
    """Test constructing a function call with a required and an optional parameter."""
    assert terramare.structure(input_value, into=_param_fn) == expected_output_value


@pytest.mark.parametrize(
    ["input_value", "error_contains"],
    [
        pytest.param({}, "int_", id="missing argument"),
        pytest.param(
            {"int_": "string"}, "expected integer", id="argument type mismatch"
        ),
        pytest.param(
            {"int_": "string", "str_": 0},
            "expected integer",
            id="multiple type mismatches",
        ),
    ],
)
def test_construct_standard_function_call_error(
    input_value: Primitive, error_contains: str
) -> None:
    """Test error cases when constructing a standard function call."""
    with raises(ConstructorError, assert_contains=error_contains):
        terramare.structure(input_value, into=_param_fn)


@pytest.mark.parametrize(
    ["fn", "input_value", "expected_output_value"],
    [
        pytest.param(
            partial(_param_fn),
            {"int_": 1, "str_": "string"},
            {"int_": 1, "str_": "string"},
            id="no args bound",
        ),
        pytest.param(
            partial(_param_fn, 1),
            {"str_": "string"},
            {"int_": 1, "str_": "string"},
            id="bound required positional arg",
        ),
        pytest.param(
            partial(_param_fn, int_=1),
            {"str_": "string"},
            {"int_": 1, "str_": "string"},
            id="bound required keyword arg",
        ),
        pytest.param(
            partial(_param_fn, str_="string"),
            {"int_": 1},
            {"int_": 1, "str_": "string"},
            id="bound optional keyword arg",
        ),
        pytest.param(
            partial(_param_fn, 1, str_="string"),
            {},
            {"int_": 1, "str_": "string"},
            id="all args bound",
        ),
    ],
)
def test_construct_partial_call(
    fn: Callable[[Primitive], Primitive],
    input_value: Primitive,
    expected_output_value: Primitive,
) -> None:
    """Test constructing a partially-applied function call."""
    assert (
        terramare.structure(input_value, into=auto(from_=OBJECT | ARRAY)(fn))
        == expected_output_value
    )


@pytest.mark.parametrize(
    ["fn", "input_value", "error_contains"],
    [
        pytest.param(
            partial(_param_fn, 1),
            {"str_": 2},
            "str",
            id="positional arg type mismatch",
        ),
        pytest.param(
            partial(_param_fn, str_="string"),
            {"int_": "oh no"},
            "int",
            id="keyword arg type mismatch",
        ),
    ],
)
def test_construct_partial_call_error(
    fn: Callable[[Primitive], Primitive],
    input_value: Primitive,
    error_contains: str,
) -> None:
    """Test error cases when constructing a partially-applied function call."""
    with raises(ConstructorError, assert_contains=error_contains):
        terramare.structure(input_value, into=auto(from_=OBJECT | ARRAY)(fn))


@auto(from_=OBJECT | ARRAY | VALUE)
def _kwargs_fn(int_: int, **kwargs: str) -> Primitive:
    return {"int_": int_, **kwargs}


@pytest.mark.parametrize(
    ["input_value", "expected_output_value"],
    [
        pytest.param({"int_": 1}, {"int_": 1}, id="no variable fields"),
        pytest.param([1], {"int_": 1}, id="no variable elements"),
        pytest.param(1, {"int_": 1}, id="leaf"),
        pytest.param(
            {"int_": 1, "str": "string"},
            {"int_": 1, "str": "string"},
            id="single variable field",
        ),
        pytest.param(
            {"int_": 1, "str1": "string", "str2": "string"},
            {"int_": 1, "str1": "string", "str2": "string"},
            id="multiple variable fields",
        ),
    ],
)
def test_construct_variable_kwarg_function_call_mainline(
    input_value: Primitive, expected_output_value: Primitive
) -> None:
    """Test constructing a function call with a variable keyword parameter."""
    assert terramare.structure(input_value, into=_kwargs_fn) == expected_output_value


@pytest.mark.parametrize(
    ["input_value", "error_contains"],
    [
        pytest.param({}, "int_", id="missing field"),
        pytest.param([], "too few elements", id="missing element"),
        pytest.param(
            {"int_": 1, "str": "string", "float": 3.14},
            "expected string",
            id="field type mismatch",
        ),
    ],
)
def test_variable_kwarg_function_call_error(
    input_value: Primitive, error_contains: str
) -> None:
    """Test error cases when constructing a variable keyword function call."""
    with raises(ConstructorError, assert_contains=error_contains):
        terramare.structure(input_value, into=_kwargs_fn)


def _mk_arg_only_fn() -> Callable[[int], Primitive]:
    """Create a function with a positional-only argument."""

    def fn(int_: int, str_: str = "", /) -> Primitive:
        return {"int_": int_, "str_": str_}

    return fn


@pytest.mark.parametrize(
    ["input_value", "expected_output_value"],
    [
        pytest.param([1], {"int_": 1, "str_": ""}, id="no optional element"),
        pytest.param(
            [1, "string"], {"int_": 1, "str_": "string"}, id="optional element"
        ),
        pytest.param(1, {"int_": 1, "str_": ""}, id="leaf"),
    ],
)
def test_construct_arg_only_function_call_mainline(
    input_value: Primitive, expected_output_value: Mapping[str, Any]
) -> None:
    """Test constructing a function call with positional-only parameters."""
    assert (
        terramare.structure(
            input_value, into=auto(from_=ARRAY | VALUE)(_mk_arg_only_fn())
        )
        == expected_output_value
    )


def test_construct_arg_only_function_call_from_object() -> None:
    """Test constructing a function call with positional-only parameters."""
    with raises(FactoryError, assert_contains="required positional-only parameter"):
        terramare.structure({}, into=auto(from_=OBJECT)(_mk_arg_only_fn()))


@auto
def _kwarg_only_fn(*, int_: int, str_: str = "") -> Primitive:
    return {"int_": int_, "str_": str_}


@pytest.mark.parametrize(
    ["input_value", "expected_output_value"],
    [
        pytest.param({"int_": 1}, {"int_": 1, "str_": ""}, id="no optional argument"),
        pytest.param(
            {"int_": 1, "str_": "string"},
            {"int_": 1, "str_": "string"},
            id="optional argument",
        ),
    ],
)
def test_construct_kwarg_only_function_call_mainline(
    input_value: Primitive, expected_output_value: Primitive
) -> None:
    """Test constructing a function call with keyword-only parameters."""
    assert (
        terramare.structure(
            input_value, into=_kwarg_only_fn, context={"hello": "world"}
        )
        == expected_output_value
    )


@pytest.mark.parametrize(
    ["input_value", "error_contains"],
    [
        pytest.param({}, "int_", id="missing argument"),
        pytest.param([], "expected object", id="list primitive"),
    ],
)
def test_construct_kwarg_only_function_call_error(
    input_value: Primitive, error_contains: str
) -> None:
    """Test error cases when constructing a keyword-only function call."""
    with raises(ConstructorError, assert_contains=error_contains):
        terramare.structure(input_value, into=_kwarg_only_fn)


@auto(from_=ARRAY | OBJECT | VALUE)
def _args_fn(int_: int, *args: str) -> Primitive:
    return {"int_": int_, "args": list(args)}


@pytest.mark.parametrize(
    ["input_value", "expected_output_value"],
    [
        pytest.param(1, {"int_": 1, "args": []}, id="leaf"),
        pytest.param([1], {"int_": 1, "args": []}, id="no variable elements"),
        pytest.param({"int_": 1}, {"int_": 1, "args": []}, id="no variable fields"),
        pytest.param([1, "a"], {"int_": 1, "args": ["a"]}, id="one variable element"),
        pytest.param(
            [1, "a", "b"],
            {"int_": 1, "args": ["a", "b"]},
            id="multiple variable elements",
        ),
    ],
)
def test_construct_variable_arg_function_call_mainline(
    input_value: Primitive, expected_output_value: Primitive
) -> None:
    """Test constructing a function call with a variable positional parameter."""
    assert terramare.structure(input_value, into=_args_fn) == expected_output_value


@pytest.mark.parametrize(
    ["input_value", "error_contains"],
    [
        pytest.param({}, "missing required field", id="dict primitive"),
        pytest.param(
            ["1"], "expected integer", id="non-variable argument type mismatch"
        ),
        pytest.param(
            [1, 2], "expected string", id="first variable argument type mismatch"
        ),
        pytest.param(
            [1, "2", 3], "expected string", id="later variable argument type mismatch"
        ),
    ],
)
def test_construct_variable_arg_function_call_error(
    input_value: Primitive, error_contains: str
) -> None:
    """Test error cases when constructing a variable positional function call."""
    with raises(ConstructorError, assert_contains=error_contains):
        terramare.structure(input_value, into=_args_fn)


@auto(from_=OBJECT | ARRAY | VALUE)
def _kwarg_arg_fn(
    int_arg: int, *args: int, int_kwarg: int = 0, **kwargs: int
) -> Primitive:
    primitives: Mapping[str, Primitive] = {
        "int_arg": int_arg,
        "args": list(args),
        "int_kwarg": int_kwarg,
        "kwargs": dict(kwargs),
    }
    return {k: v for k, v in primitives.items() if v}


@pytest.mark.parametrize(
    ["input_value", "expected_output_value"],
    [
        pytest.param([1], {"int_arg": 1}, id="minimal array"),
        pytest.param({"int_arg": 1}, {"int_arg": 1}, id="minimal object"),
        pytest.param(1, {"int_arg": 1}, id="leaf"),
        pytest.param(
            {"int_arg": 1, "int_kwarg": 2},
            {"int_arg": 1, "int_kwarg": 2},
            id="non-variable arg and kwarg",
        ),
        pytest.param([1, 2, 3], {"int_arg": 1, "args": [2, 3]}, id="variable args"),
        pytest.param(
            {"int_arg": 0, "int_kwarg": 1, "two": 2, "three": 3},
            {"int_kwarg": 1, "kwargs": {"two": 2, "three": 3}},
            id="variable and non-variable kwargs",
        ),
        pytest.param(
            {"int_arg": 0, "two": 2, "three": 3},
            {"kwargs": {"two": 2, "three": 3}},
            id="variable kwargs only",
        ),
    ],
)
def test_construct_variable_arg_kwarg_function_call_mainline(
    input_value: Primitive, expected_output_value: Primitive
) -> None:
    """Test constructing a function call with variable positional and keyword parameters."""
    assert terramare.structure(input_value, into=_kwarg_arg_fn) == expected_output_value


class _named_tuple(NamedTuple):
    int_: int
    str_: str = ""


@pytest.mark.parametrize(
    ["input_value", "expected_output_value"],
    [
        pytest.param(
            {"int_": 1}, _named_tuple(1), id="from dict, no optional argument"
        ),
        pytest.param([1], _named_tuple(1), id="from list, no optional argument"),
        pytest.param(
            {"int_": 1, "str_": "string"},
            _named_tuple(1, "string"),
            id="from dict, optional argument",
        ),
        pytest.param(
            [1, "string"], _named_tuple(1, "string"), id="from list, optional argument"
        ),
    ],
)
def test_construct_namedtuple_mainline(
    input_value: Primitive, expected_output_value: _named_tuple
) -> None:
    """Test constructing a NamedTuple."""
    assert terramare.structure(input_value, into=_named_tuple) == expected_output_value


@dataclass
class _dataclass:
    int_: int
    str_: str = ""


@pytest.mark.parametrize(
    ["input_value", "expected_output_value"],
    [
        pytest.param({"int_": 1}, _dataclass(1), id="from dict, no optional argument"),
        pytest.param(
            {"int_": 1, "str_": "string"},
            _dataclass(1, "string"),
            id="from dict, optional argument",
        ),
    ],
)
def test_construct_dataclass_mainline(
    input_value: Primitive, expected_output_value: _dataclass
) -> None:
    """Test constructing a class defined using dataclasses."""
    assert terramare.structure(input_value, into=_dataclass) == expected_output_value


@attr.s(auto_attribs=True, frozen=True)
class _attr_class:
    int_: int
    str_: str = ""


@pytest.mark.parametrize(
    ["input_value", "expected_output_value"],
    [
        pytest.param({"int_": 1}, _attr_class(1), id="from dict, no optional argument"),
        pytest.param(
            {"int_": 1, "str_": "string"},
            _attr_class(1, "string"),
            id="from dict, optional argument",
        ),
    ],
)
def test_construct_attrs_class_mainline(
    input_value: Primitive, expected_output_value: _attr_class
) -> None:
    """Test constructing a class defined using the attrs library."""
    assert terramare.structure(input_value, into=_attr_class) == expected_output_value


@auto(from_=OBJECT | ARRAY)
class _complex_class:
    def __init__(self, a: _named_tuple, b: _attr_class):
        self.a = a
        self.b = b
        self.c = "c"

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, type(self)):
            return NotImplemented
        return vars(self) == vars(other)

    def __repr__(self) -> str:
        return "_complex_class({})".format(  # pylint: disable=consider-using-f-string
            ", ".join([f"{k}={v}" for k, v in vars(self).items()])
        )


@pytest.mark.parametrize(
    ["input_value", "expected_output_value"],
    [
        pytest.param(
            {"a": {"int_": 1}, "b": {"int_": 2, "str_": "string"}},
            _complex_class(_named_tuple(1), _attr_class(2, "string")),
            id="mainline",
        )
    ],
)
def test_construct_complex_class_mainline(
    input_value: Primitive, expected_output_value: Primitive
) -> None:
    """Test constructing a complex class."""
    assert (
        terramare.structure(input_value, into=_complex_class) == expected_output_value
    )


@pytest.mark.parametrize(
    ["value_fn"],
    [
        pytest.param(lambda v: v, id="from leaf"),
        pytest.param(lambda v: [v], id="from array"),
        pytest.param(lambda v: {"i": v}, id="from object"),
    ],
)
def test_handle_single_exception_type(value_fn: Callable[[int], Primitive]) -> None:
    """Test catching and handling a single additional exception type."""

    @handle_exception_types(ValueError)
    @auto(from_=OBJECT | ARRAY | VALUE)
    def _handle_single_exception_fn(i: int) -> None:
        if i == 0:
            raise ValueError("value_error")
        if i == 1:
            raise KeyError("key_error")

    with raises(ConstructorError, assert_contains="value_error"):
        terramare.structure(
            value_fn(0),
            into=_handle_single_exception_fn,
        )

    # A KeyError, not listed in handle_exception_ts, should not be caught by
    # terramare.
    with raises(KeyError, assert_contains="key_error"):
        terramare.structure(
            value_fn(1),
            into=_handle_single_exception_fn,
        )


@handle_exception_types(ValueError, KeyError)
@auto(from_=OBJECT | ARRAY | VALUE)
def _handle_multiple_exceptions_fn(i: int) -> None:
    if i == 0:
        raise ValueError("value_error")
    if i == 1:
        raise KeyError("key_error")


@pytest.mark.parametrize(
    ["value_fn"],
    [
        pytest.param(lambda v: v, id="from leaf"),
        pytest.param(lambda v: [v], id="from array"),
        pytest.param(lambda v: {"i": v}, id="from object"),
    ],
)
def test_handle_multiple_exception_types(value_fn: Callable[[int], Primitive]) -> None:
    """Test catching and handling multiple additional exception type."""

    with raises(ConstructorError, assert_contains="value_error"):
        terramare.structure(
            value_fn(0),
            into=_handle_multiple_exceptions_fn,
        )

    with raises(ConstructorError, assert_contains="key_error"):
        terramare.structure(
            value_fn(1),
            into=_handle_multiple_exceptions_fn,
        )


def test_not_a_callable_error() -> None:
    """Test creating a class constructor from a non-callable."""
    with pytest.raises(UnsupportedTargetTypeError):
        test_utils.create_constructor_internal(  # type: ignore[arg-type]
            classes.ClassFactory(MetadataCollection()), 0
        )


def test_non_constructible_parameter_type_fn() -> None:
    """Test a function with a non-constructible parameter type."""

    @auto
    def fn(_arg: test_utils.NonConstructible) -> None:
        pass

    with raises(FactoryError, assert_contains="NonConstructible"):
        terramare.structure({}, into=fn)


def test_no_signature() -> None:
    """Test a function for which the inspect module cannot obtain a signature."""
    with raises(FactoryError, assert_contains="str"):
        test_utils.create_constructor_internal(
            classes.ClassFactory(MetadataCollection()), str
        )


def test_construct_zero_parameter_function_call_from_value() -> None:
    """Test constructing a function call with no parameters from a value."""

    @auto(from_=VALUE)
    def _no_param_fn_value() -> None:
        pass

    with raises(FactoryError, assert_contains="target with no positional parameters"):
        terramare.structure({}, into=_no_param_fn_value)


def test_no_auto() -> None:
    """Test constructing a function call that is not marked with auto."""

    def _no_auto_fn() -> None:
        pass

    with raises(FactoryError, assert_contains="not enabled for target"):
        terramare.structure({}, into=_no_auto_fn)


def test_auto_false() -> None:
    """Test constructing a function call that is marked auto with no FromTypes."""

    @auto(from_=frozenset())
    def _auto_no_fromtypes() -> None:
        pass

    with raises(FactoryError, assert_contains="disabled for target"):
        terramare.structure({}, into=_auto_no_fromtypes)


def test_keyword_only_from_value() -> None:
    """Test constructing a function call with keyword-only parameters from a value."""

    @auto(from_=VALUE)
    def _keyword_only_from_value(_: int, *, _param: int) -> None:
        pass

    with raises(FactoryError, assert_contains="required keyword-only parameter"):
        terramare.structure({}, into=_keyword_only_from_value)


def test_keyword_only_from_object() -> None:
    """Test constructing a function call with keyword-only parameters from an array."""

    @auto(from_=ARRAY)
    def _keyword_only_from_array(*, _param: int) -> None:
        pass

    with raises(FactoryError, assert_contains="required keyword-only parameter"):
        terramare.structure({}, into=_keyword_only_from_array)


def test_no_positional_from_value() -> None:
    """Test constructing a function call with no positional parameters from a value."""

    @auto(from_=VALUE)
    def _no_positional_from_value(*, _param: int = 0) -> None:
        pass

    with raises(FactoryError, assert_contains="no positional parameters"):
        terramare.structure({}, into=_no_positional_from_value)


def test_multiple_required_from_value() -> None:
    """Test constructing a function call with multiple required parameters from a value."""

    @auto(from_=VALUE)
    def _multiple_required_from_value(_x: int, _y: int) -> None:
        pass

    with raises(FactoryError, assert_contains="more than one required parameter"):
        terramare.structure({}, into=_multiple_required_from_value)


def test_args_from_value() -> None:
    """Test constructing function with variable positional arguments from a value."""

    @auto(from_=VALUE)
    def _args_from_value(*args: int) -> Tuple[int, ...]:
        return args

    assert terramare.structure(1, into=_args_from_value) == (1,)
