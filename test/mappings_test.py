"""Tests for constructing mapping types."""

from typing import Dict, List, Mapping, MutableMapping, Tuple

import pytest

from terramare import mappings, terramare
from terramare.errors import ConstructorError, FactoryError, UnsupportedTargetTypeError
from terramare.types import Primitive

from . import test_utils
from .test_utils import raises


@pytest.mark.parametrize(
    ["dict_t", "input_value"],
    [
        pytest.param(Dict, {"a": 1}, id="unspecified type dict"),
        pytest.param(Dict[str, int], {}, id="empty dict"),
        pytest.param(Dict[str, int], {"a": 1, "b": 2}, id="mainline dict"),
        pytest.param(Mapping, {"a": 1}, id="unspecified type mapping"),
        pytest.param(Mapping[str, int], {}, id="empty mapping"),
        pytest.param(Mapping[str, int], {"a": 1, "b": 2}, id="mainline mapping"),
        pytest.param(MutableMapping, {"a": 1}, id="unspecified type mutable mapping"),
        pytest.param(MutableMapping[str, int], {}, id="empty mutable mapping"),
        pytest.param(
            MutableMapping[str, int], {"a": 1, "b": 2}, id="mainline mutable mapping"
        ),
    ],
)
def test_construct_dict_mainline(dict_t: type, input_value: Primitive) -> None:
    """Test that a dictionary can be constructed."""
    assert terramare.structure(input_value, into=dict_t) == input_value  # type: ignore


@pytest.mark.parametrize(
    ["dict_t", "input_value", "error_contains"],
    [
        pytest.param(Dict[str, int], 0, ["object"], id="not a dict"),
        pytest.param(
            Dict[str, int],
            {"a": "1", "b": "2"},
            ['"a"', '"b"'],
            id="dict value type mismatch",
        ),
        pytest.param(
            Mapping[str, int],
            {"a": "1", "b": "2"},
            ['"a"', '"b"'],
            id="mapping value type mismatch",
        ),
        pytest.param(
            MutableMapping[str, int], 0, ["object"], id="not a mutable mapping"
        ),
        pytest.param(
            MutableMapping[str, int],
            {"a": "1", "b": "2"},
            ['"a"', '"b"'],
            id="mutable mapping value type mismatch",
        ),
    ],
)
def test_construct_dict_error(
    dict_t: type, input_value: Primitive, error_contains: List[str]
) -> None:
    """Test that a descriptive error is raised if a dict cannot be constructed."""
    with raises(ConstructorError, assert_contains=error_contains):
        terramare.structure(input_value, into=dict_t)


def test_not_a_dict_error() -> None:
    """Test that an error is raised when creating a dict constructor from a non-dict type."""
    with pytest.raises(UnsupportedTargetTypeError):
        test_utils.create_constructor_internal(
            mappings.MappingFactory(), Tuple[int, int]
        )


def test_non_string_key_type_error() -> None:
    """Test that an error is raised if the key type is not str."""
    with raises(FactoryError, assert_contains="only dictionary keys of type str"):
        terramare.structure({}, into=Dict[int, int])


def test_non_constructible_element_type() -> None:
    """Test that an error is raised if the key or value type is not constructible."""
    with raises(FactoryError, assert_contains="NonConstructible"):
        terramare.structure({}, into=Dict[str, test_utils.NonConstructible])
