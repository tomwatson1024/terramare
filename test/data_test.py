"""Tests for terramare.data module."""

import pytest

from terramare import data


def test_context_getitem_ok() -> None:
    assert data.Context({"a": 0, "b": 1, "c": 2})["b"] == 1


def test_context_getitem_err() -> None:
    with pytest.raises(KeyError) as excinfo:
        _ = data.Context({"a": 0, "b": 1, "c": 2})["d"]
    assert excinfo.value.args == ("d",)


def test_context_iter() -> None:
    assert list(data.Context({"a": 0, "b": 1, "c": 2})) == ["a", "b", "c"]


def test_context_len() -> None:
    assert len(data.Context({"a": 0, "b": 1, "c": 2})) == 3


def test_array_index_int_ok() -> None:
    assert data.Array.new(["a", "b", "c"])[1].raw == "b"


def test_array_index_int_err() -> None:
    with pytest.raises(IndexError):
        _ = data.Array.new(["a", "b", "c"])[3]


def test_array_index_slice() -> None:
    assert ([e.raw for e in data.Array.new(["a", "b", "c"])[0:2]]) == ["a", "b"]


def test_object_eq_ok() -> None:
    assert data.Object.new({"a": 0}) == data.Object.new({"a": 0})


def test_object_eq_key_mismatch_err() -> None:
    assert data.Object.new({"a": 0}) != data.Object.new({"b": 0})


def test_object_eq_value_mismatch_err() -> None:
    assert data.Object.new({"a": 0}) != data.Object.new({"a": 1})


def test_object_get_ok() -> None:
    assert data.Object.new({"a": 0, "b": 1, "c": 2})["b"].raw == 1


def test_object_getitem_err() -> None:
    with pytest.raises(KeyError) as excinfo:
        _ = data.Object.new({"a": 0, "b": 1, "c": 2})["d"]
    assert excinfo.value.args == ("d",)


def test_object_pop_ok() -> None:
    object_ = data.Object.new({"a": 0, "b": 1, "c": 2})
    object_, value = object_.pop("b")
    assert object_ == data.Object.new({"a": 0, "c": 2})
    assert value.raw == 1


def test_object_pop_err() -> None:
    object_ = data.Object.new({"a": 0, "b": 1, "c": 2})
    with pytest.raises(KeyError) as excinfo:
        object_.pop("z")
    assert excinfo.value.args == ("z",)


def test_object_iter() -> None:
    assert list(data.Object.new({"a": 0, "b": 1, "c": 2})) == ["a", "b", "c"]


def test_object_len() -> None:
    assert len(data.Object.new({"a": 0, "b": 1, "c": 2})) == 3
