"""Tests for `disallow_unknown_fields` metadata."""

from dataclasses import dataclass

import terramare

from .test_utils import raises


def test_allow_unknown_fields_by_default() -> None:
    @dataclass(frozen=True)
    class FromObject:
        pass

    assert terramare.structure({"foo": ""}, into=FromObject) == FromObject()


def test_disallow_unknown_fields_no_params() -> None:
    @terramare.disallow_unknown_fields
    @dataclass(frozen=True)
    class FromObject:
        pass

    with raises(terramare.ConstructorError, assert_contains='unknown field(s): "foo"'):
        terramare.structure({"foo": ""}, into=FromObject)


@terramare.disallow_unknown_fields(excluding={"foo"})
@dataclass(frozen=True)
class FromObjectExcludingFoo:
    pass


def test_disallow_unknown_fields_excluding_ok() -> None:
    assert (
        terramare.structure({"foo": ""}, into=FromObjectExcludingFoo)
        == FromObjectExcludingFoo()
    )


def test_disallow_unknown_fields_excluding_err() -> None:
    with raises(terramare.ConstructorError, assert_contains='unknown field(s): "bar"'):
        assert (
            terramare.structure({"bar": ""}, into=FromObjectExcludingFoo)
            == FromObjectExcludingFoo()
        )


@terramare.disallow_unknown_fields(exclude_if=lambda s: s.startswith("_"))
@dataclass(frozen=True)
class FromObjectExcludeIfUnderscore:
    pass


def test_disallow_unknown_fields_exclude_if_ok() -> None:
    assert (
        terramare.structure({"_foo": ""}, into=FromObjectExcludeIfUnderscore)
        == FromObjectExcludeIfUnderscore()
    )


def test_disallow_unknown_fields_exclude_if_err() -> None:
    with raises(terramare.ConstructorError, assert_contains='unknown field(s): "foo"'):
        assert (
            terramare.structure({"foo": ""}, into=FromObjectExcludeIfUnderscore)
            == FromObjectExcludeIfUnderscore()
        )


def test_allow_extra_elements_by_default() -> None:
    @terramare.auto(from_=terramare.ARRAY)
    @dataclass(frozen=True)
    class FromArray:
        pass

    assert terramare.structure(["foo"], into=FromArray) == FromArray()


def test_disallow_extra_elements_no_params() -> None:
    @terramare.disallow_unknown_fields
    @terramare.auto(from_=terramare.ARRAY)
    @dataclass(frozen=True)
    class FromArray:
        pass

    with raises(terramare.ConstructorError, assert_contains="too many elements"):
        terramare.structure(["foo"], into=FromArray)


def test_disallow_extra_elements_excluding() -> None:
    @terramare.disallow_unknown_fields(excluding={"foo"})
    @terramare.auto(from_=terramare.ARRAY)
    @dataclass(frozen=True)
    class FromArray:
        pass

    with raises(terramare.ConstructorError, assert_contains="too many elements"):
        terramare.structure(["foo"], into=FromArray)


def test_disallow_extra_elements_exclude_if() -> None:
    @terramare.disallow_unknown_fields(exclude_if=lambda s: s.startswith("_"))
    @terramare.auto(from_=terramare.ARRAY)
    @dataclass(frozen=True)
    class FromArray:
        pass

    with raises(terramare.ConstructorError, assert_contains="too many elements"):
        terramare.structure(["foo"], into=FromArray)
