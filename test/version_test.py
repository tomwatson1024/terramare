"""Test that package version numbers match."""

import pathlib

import toml

import terramare

PYPROJECT_TOML = pathlib.Path(__file__).parent.parent / "pyproject.toml"


def test_versions_match() -> None:
    """
    Test that the version number in __init__.py is up-to-date.

    pyproject.toml is the authoritative source for the version number.
    """
    with open(PYPROJECT_TOML, encoding="utf-8") as f:
        pyproject_version = toml.load(f)["tool"]["poetry"]["version"]
    assert terramare.__version__ == pyproject_version
