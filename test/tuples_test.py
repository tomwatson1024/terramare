"""Tests for constructing Tuple types."""

from typing import Any, List, Tuple

import pytest

from terramare import sequences, terramare
from terramare.errors import ConstructorError, FactoryError, UnsupportedTargetTypeError
from terramare.types import Primitive

from . import test_utils
from .test_utils import raises


@pytest.mark.parametrize(
    ["tuple_t", "input_value", "expected_output_value"],
    [
        pytest.param(Tuple[()], [], (), id="empty tuple"),
        pytest.param(Tuple[Tuple[()]], [[]], ((),), id="nested empty tuple"),
        pytest.param(Tuple[str], ["string"], ("string",), id="singleton"),
        pytest.param(Tuple[str, str], ["a", "b"], ("a", "b"), id="homogeneous pair"),
        pytest.param(Tuple[str, int], ["a", 1], ("a", 1), id="hetrogeneous pair"),
        pytest.param(Tuple[str, ...], ["a"], ("a",), id="one element variadic"),
        pytest.param(
            Tuple[str, ...], ["a", "b"], ("a", "b"), id="multiple element variadic"
        ),
        pytest.param(Tuple, ["a", "b"], ("a", "b"), id="untyped Tuple"),
        pytest.param(tuple, ["a", "b"], ("a", "b"), id="untyped tuple"),
    ],
)
def test_construct_tuple_mainline(
    tuple_t: type, input_value: Primitive, expected_output_value: Any
) -> None:
    """Test that a (typed) tuple can be constructed."""
    assert terramare.structure(input_value, into=tuple_t) == expected_output_value


@pytest.mark.parametrize(
    ["tuple_t", "input_value", "error_contains"],
    [
        pytest.param(Tuple[str], "string", "expected array", id="primitive not a list"),
        pytest.param(Tuple[str], [], "too few elements", id="too few elements"),
        pytest.param(Tuple[()], [1], "too many elements", id="too many elements"),
        pytest.param(
            Tuple[str, int], [1, "string"], "expected string", id="type mismatch"
        ),
    ],
)
def test_construct_tuple_error(
    tuple_t: type, input_value: Primitive, error_contains: str
) -> None:
    """Test that an appropriate error is raised if a tuple cannot be constructed."""
    with raises(ConstructorError, assert_contains=error_contains):
        terramare.structure(input_value, into=tuple_t)


def test_not_a_tuple_error() -> None:
    """Test that an error is raised when creating a tuple constructor from a non-tuple type."""
    with pytest.raises(UnsupportedTargetTypeError):
        test_utils.create_constructor_internal(sequences.TupleFactory(), List[int])


def test_non_constructible_element_type() -> None:
    """Test that an error is raised if element types are not constructible."""
    with raises(FactoryError, assert_contains="NonConstructible"):
        terramare.structure({}, into=Tuple[test_utils.NonConstructible])
