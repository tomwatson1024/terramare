"""Tests for from_context metadata."""

from dataclasses import dataclass

import terramare
import terramare.errors

from .test_utils import raises


def test_from_context_missing_err() -> None:
    @terramare.fields({"x": terramare.from_context("x")})
    @dataclass(frozen=True)
    class X:
        x: int

    with raises(
        terramare.errors.ContextTypeError,
        assert_contains="missing context parameter 'x'",
    ):
        terramare.structure({}, into=X)
