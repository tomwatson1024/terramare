"""Tests for constructing function types with context parameters."""

from typing import Optional

import pytest

import terramare
from terramare import ARRAY, OBJECT, VALUE, Context, auto
from terramare.types import Target

from .test_utils import raises


def test_context_object() -> None:
    @auto
    def fn(
        ctx_1: Context, *args: Context, ctx_2: Context, int_: int, **kwargs: Context
    ) -> object:
        return {
            "ctx_1": dict(ctx_1),
            "args": args,
            "ctx_2": dict(ctx_2),
            "int_": int_,
            "kwargs": kwargs,
        }

    assert terramare.structure({"int_": 1}, into=fn, context={"hello": "world"}) == {
        "ctx_1": {"hello": "world"},
        "args": (),
        "ctx_2": {"hello": "world"},
        "int_": 1,
        "kwargs": {},
    }


def test_context_object_positional_only() -> None:
    def fn(
        ctx_1: Context,
        /,
        ctx_2: Context,
        *args: Context,
        ctx_3: Context,
        int_: int,
        **kwargs: Context,
    ) -> object:
        return {
            "ctx_1": dict(ctx_1),
            "ctx_2": dict(ctx_2),
            "args": args,
            "ctx_3": dict(ctx_3),
            "int_": int_,
            "kwargs": kwargs,
        }

    assert terramare.structure(
        {"int_": 1},
        into=auto(from_=OBJECT)(fn),
        context={"hello": "world"},
    ) == {
        "ctx_1": {"hello": "world"},
        "ctx_2": {"hello": "world"},
        "args": (),
        "ctx_3": {"hello": "world"},
        "int_": 1,
        "kwargs": {},
    }


def test_context_array() -> None:
    @auto(from_=ARRAY)
    def fn(ctx_1: Context, *args: Context, ctx_2: Context, **kwargs: Context) -> object:
        return {
            "ctx_1": dict(ctx_1),
            "args": args,
            "ctx_2": dict(ctx_2),
            "kwargs": kwargs,
        }

    assert terramare.structure([], into=fn, context={"hello": "world"}) == {
        "ctx_1": {"hello": "world"},
        "args": (),
        "ctx_2": {"hello": "world"},
        "kwargs": {},
    }


def test_context_array_positional_only() -> None:
    def fn(
        int_: int,
        ctx_1: Context,
        /,
        ctx_2: Context,
        *args: Context,
        ctx_3: Context,
        **kwargs: Context,
    ) -> object:
        return {
            "int_": int_,
            "ctx_1": dict(ctx_1),
            "ctx_2": dict(ctx_2),
            "args": args,
            "ctx_3": dict(ctx_3),
            "kwargs": kwargs,
        }

    assert terramare.structure(
        [1], into=auto(from_=ARRAY)(fn), context={"hello": "world"}
    ) == {
        "int_": 1,
        "ctx_1": {"hello": "world"},
        "ctx_2": {"hello": "world"},
        "args": (),
        "ctx_3": {"hello": "world"},
        "kwargs": {},
    }


def test_context_value() -> None:
    @auto(from_=VALUE)
    def fn(
        int_: int,
        ctx_1: Context,
        *args: Context,
        ctx_2: Context,
        **kwargs: Context,
    ) -> object:
        return {
            "int_": int_,
            "ctx_1": dict(ctx_1),
            "args": args,
            "ctx_2": dict(ctx_2),
            "kwargs": kwargs,
        }

    assert terramare.structure(1, into=fn, context={"hello": "world"}) == {
        "int_": 1,
        "ctx_1": {"hello": "world"},
        "args": (),
        "ctx_2": {"hello": "world"},
        "kwargs": {},
    }


def test_context_value_positional_only() -> None:
    def fn(
        ctx_1: Context,
        /,
        int_: int,
        ctx_2: Context,
        *args: Context,
        ctx_3: Context,
        **kwargs: Context,
    ) -> object:
        return {
            "ctx_1": dict(ctx_1),
            "int_": int_,
            "ctx_2": dict(ctx_2),
            "args": args,
            "ctx_3": dict(ctx_3),
            "kwargs": kwargs,
        }

    assert terramare.structure(
        1,
        into=auto(from_=VALUE)(fn),
        context={"hello": "world"},
    ) == {
        "ctx_1": {"hello": "world"},
        "int_": 1,
        "ctx_2": {"hello": "world"},
        "args": (),
        "ctx_3": {"hello": "world"},
        "kwargs": {},
    }


@pytest.mark.parametrize(
    "type_",
    [
        pytest.param(Optional[Context], id="generic type parameter"),
    ],
)
def test_context_bad_placement(type_: Target[object]) -> None:
    with raises(
        terramare.FactoryError,
        assert_contains="construction explicitly disabled for this type",
    ):
        terramare.structure(
            {},
            into=type_,
        )
