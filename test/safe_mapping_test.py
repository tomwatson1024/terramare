"""Tests for terramare.safe_mapping module."""

from typing import Any, Iterable, Mapping, MutableMapping, Tuple, TypeVar

import pytest
from typing_extensions import Protocol

from terramare.safe_mapping import (
    EqMapping,
    EqMutableMapping,
    SafeMapping,
    SafeMutableMapping,
)

_K_in = TypeVar("_K_in")

_V_co = TypeVar("_V_co", covariant=True)


class MappingFactory(Protocol):
    def __call__(
        self, __it: Iterable[Tuple[_K_in, _V_co]] = ()
    ) -> Mapping[_K_in, _V_co]: ...


@pytest.fixture(
    params=[dict, EqMapping, EqMutableMapping, SafeMapping, SafeMutableMapping],
    name="mapping",
)
def mapping_fixture(
    request: Any,
) -> MappingFactory:
    return request.param


def test_mapping_len_empty(mapping: MappingFactory) -> None:
    m: Mapping[str, int] = mapping()
    assert len(m) == 0


def test_mapping_len_mainline(mapping: MappingFactory) -> None:
    m = mapping([("one", 1), ("two", 2)])
    assert len(m) == 2


def test_mapping_len_duplicates(mapping: MappingFactory) -> None:
    m = mapping([("one", 1), ("two", 2), ("one", 1)])
    assert len(m) == 2


def test_mapping_iter_empty(mapping: MappingFactory) -> None:
    m: Mapping[str, int] = mapping()
    assert not list(m)


def test_mapping_iter_mainline(mapping: MappingFactory) -> None:
    m = mapping([("one", 1), ("two", 2)])
    assert list(m) == ["one", "two"]


def test_mapping_iter_duplicates(mapping: MappingFactory) -> None:
    m = mapping([("one", 1), ("two", 2), ("one", 1)])
    assert list(m) == ["one", "two"]


def test_mapping_getitem_mainline(mapping: MappingFactory) -> None:
    m = mapping([("one", 1), ("two", 2)])
    assert m["one"] == 1
    assert m["two"] == 2


def test_mapping_getitem_error(mapping: MappingFactory) -> None:
    m = mapping([("one", 1)])
    with pytest.raises(KeyError) as exc_info:
        m["two"]  # pylint: disable=pointless-statement
    assert exc_info.value.args == ("two",)


class MutableMappingFactory(Protocol):
    def __call__(
        self, __it: Iterable[Tuple[_K_in, _V_co]] = ()
    ) -> MutableMapping[_K_in, _V_co]: ...


@pytest.fixture(
    params=[dict, EqMutableMapping, SafeMutableMapping], name="mutable_mapping"
)
def mutable_mapping_fixture(
    request: Any,
) -> MutableMappingFactory:
    return request.param


def test_mutable_mapping_setitem_mainline(
    mutable_mapping: MutableMappingFactory,
) -> None:
    m = mutable_mapping([("one", 1)])
    with pytest.raises(KeyError):
        m["two"]  # pylint: disable=pointless-statement
    m["two"] = 2
    assert m["two"] == 2


def test_mutable_mapping_setitem_already_present(
    mutable_mapping: MutableMappingFactory,
) -> None:
    m = mutable_mapping([("one", 1), ("two", 2)])
    assert m["two"] == 2
    m["two"] = 3
    assert m["two"] == 3


def test_mutable_mapping_delitem_mainline(
    mutable_mapping: MutableMappingFactory,
) -> None:
    m = mutable_mapping([("one", 1), ("two", 2)])
    assert m["two"] == 2
    del m["two"]
    with pytest.raises(KeyError):
        m["two"]  # pylint: disable=pointless-statement


def test_mutable_mapping_delitem_error(
    mutable_mapping: MutableMappingFactory,
) -> None:
    m = mutable_mapping([("one", 1)])
    with pytest.raises(KeyError):
        m["two"]  # pylint: disable=pointless-statement
    with pytest.raises(KeyError) as exc_info:
        del m["two"]
    assert exc_info.value.args == ("two",)
    with pytest.raises(KeyError):
        m["two"]  # pylint: disable=pointless-statement


class SafeMappingFactory(Protocol):
    def __call__(
        self, __it: Iterable[Tuple[_K_in, _V_co]] = ()
    ) -> SafeMapping[_K_in, _V_co]: ...


@pytest.fixture(params=[SafeMapping, SafeMutableMapping], name="safe_mapping")
def safe_mapping_fixture(
    request: Any,
) -> MutableMappingFactory:
    return request.param


def test_safe_mapping_getitem_mainline(safe_mapping: SafeMappingFactory) -> None:
    m = safe_mapping([([1], 1), ("two", 2)])
    assert m[[1]] == 1
    assert m["two"] == 2


def test_safe_mapping_getitem_error(safe_mapping: SafeMappingFactory) -> None:
    m = safe_mapping([([1], 1), ("two", 2)])
    with pytest.raises(KeyError) as exc_info:
        m[[2]]  # pylint: disable=pointless-statement
    assert exc_info.value.args == ([2],)


def test_safe_mapping_len(safe_mapping: SafeMappingFactory) -> None:
    m = safe_mapping([([1], 1), ("two", 2)])
    assert len(m) == 2


def test_safe_mapping_iter(safe_mapping: SafeMappingFactory) -> None:
    m = safe_mapping([([1], 1), ("two", 2), ([3], "three")])
    # As a consequence of the implementation hashable keys are yielded first,
    # followed by unhashable keys.
    # This behaviour is not particularly important, but make sure it doesn't
    # change without notice.
    assert list(m) == ["two", [1], [3]]


def test_safe_mutable_mapping_setitem_mainline() -> None:
    m = SafeMutableMapping([([1], 1), ("two", 2)])
    with pytest.raises(KeyError):
        m[[3]]  # pylint: disable=pointless-statement
    m[[3]] = 3
    assert m[[3]] == 3


def test_safe_mutable_mapping_setitem_already_present() -> None:
    m = SafeMutableMapping([([1], 1), ("two", 2)])
    m[[1]] = 3
    assert m[[1]] == 3


def test_safe_mutable_mapping_delitem_mainline() -> None:
    m = SafeMutableMapping([([1], 1), ("two", 2)])
    assert m[[1]] == 1
    del m[[1]]
    with pytest.raises(KeyError):
        m[[1]]  # pylint: disable=pointless-statement


def test_safe_mutable_mapping_delitem_error() -> None:
    m = SafeMutableMapping([([1], 1), ("two", 2)])
    with pytest.raises(KeyError):
        m[[3]]  # pylint: disable=pointless-statement
    with pytest.raises(KeyError) as exc_info:
        del m[[3]]
    assert exc_info.value.args == ([3],)
    with pytest.raises(KeyError):
        m[[3]]  # pylint: disable=pointless-statement
