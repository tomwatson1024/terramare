.. image:: _static/emoji.png

terramare
=========

|python_badge| |version_badge| |license_badge|

.. |license_badge| image:: https://img.shields.io/badge/license-MIT-blueviolet.svg
   :target: https://opensource.org/licenses/MIT
   :alt: licence: MIT
.. |version_badge| image:: https://img.shields.io/pypi/v/terramare
   :target: https://pypi.org/project/terramare/
   :alt: PyPI
.. |python_badge| image:: https://img.shields.io/badge/python-3.8%2B-blue
   :target: https://www.python.org/
   :alt: python: 3.8+

Automatically construct complex objects from simple Python types.

Highlights
----------

No boilerplate
  **terramare** uses Python's standard type hints to determine how to construct
  instances of a class;
Format-agnostic
   **terramare** takes simple Python types as input - pass it the output from
   ``json.load``, ``toml.load``, or ``yaml.load``;
Non-invasive
   **terramare** requires no modifications to your existing classes and
   functions beyond standard type hints.

Guide
-----

.. toctree::
   :maxdepth: 2

   usage
   api
   genindex
   Source Code <https://gitlab.com/tomwatson1024/terramare>

Alternatives
------------

Check out:

- `pydantic <https://pydantic-docs.helpmanual.io/>`_ - *"Data validation and settings management using python type annotations"*. A much more mature library also using Python's standard type hints for deserialization that requires a little more integration with your code;
- `schematics <https://schematics.readthedocs.io/en/latest/>`_ - *"...combine types into structures, validate them, and transform the shapes of your data based on simple descriptions"*. Uses custom types instead of Python's standard type hints;
- `cerberus <https://docs.python-cerberus.org/en/stable/>`_ - *"...provides powerful yet simple and lightweight data validation functionality out of the box and is designed to be easily extensible, allowing for custom validation"*. Schema validation that doesn't change the type of the primitive value.
