"""Tests for terramare.iterator_utils module."""

from terramare import iterator_utils


def test_pop_all() -> None:
    elements = [1, 2, 3, 4, 5]
    assert iterator_utils.pop_all(elements, lambda x: x % 2 == 0) == [2, 4]
    assert elements == [1, 3, 5]


def test_pop_first_with_match() -> None:
    elements = [1, 2, 3, 4, 5]
    assert iterator_utils.pop_first(elements, lambda x: x == 2) == 2
    assert elements == [1, 3, 4, 5]


def test_pop_first_without_match() -> None:
    elements = [1, 2, 3, 4, 5]
    assert iterator_utils.pop_first(elements, lambda x: x == 6) is None
    assert elements == [1, 2, 3, 4, 5]
