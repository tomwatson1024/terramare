"""Tests for constructing class/function types from objects."""

from dataclasses import InitVar, dataclass, field
from typing import Any, Callable, ClassVar, Dict, List, Mapping, Optional

import pytest

import terramare
from terramare import OBJECT
from terramare.types import Primitive

from .test_utils import raises


def test_zero_field_dataclass_ok() -> None:
    @dataclass(frozen=True)
    class Data:
        pass

    assert terramare.structure({}, into=Data) == Data()


@pytest.mark.parametrize(
    "decorator",
    [
        pytest.param(terramare.auto(False), id="auto(False)"),
        pytest.param(terramare.auto(from_=set()), id="auto(from_={})"),
    ],
)
def test_zero_field_dataclass_disabled_err(decorator: Callable[[Any], Any]) -> None:
    @decorator
    @dataclass(frozen=True)
    class Data:
        pass

    with raises(
        terramare.FactoryError, assert_contains="class construction disabled for target"
    ):
        terramare.structure({}, into=Data)


@pytest.mark.parametrize(
    "decorator",
    [
        pytest.param(lambda c: c, id="id"),
        pytest.param(terramare.auto, id="auto"),
        pytest.param(terramare.auto(True), id="auto(True)"),
        pytest.param(terramare.auto(from_=OBJECT), id="auto(from_=OBJECT)"),
    ],
)
def test_primitive_field_dataclass_ok(decorator: Callable[[Any], Any]) -> None:
    @decorator
    @dataclass(frozen=True)
    class Data:
        x: int

    assert terramare.structure({"x": 1}, into=Data) == Data(1)


def test_complex_field_dataclass_ok() -> None:
    @dataclass(frozen=True)
    class Data:
        x: Mapping[str, int]

    assert terramare.structure({"x": {"y": 1}}, into=Data) == Data({"y": 1})


@pytest.mark.parametrize(["primitive", "expected_x"], [({}, 0), ({"x": 1}, 1)])
def test_defaulted_field_dataclass_ok(primitive: Primitive, expected_x: int) -> None:
    @dataclass(frozen=True)
    class Data:
        x: int = 0

    assert terramare.structure(primitive, into=Data) == Data(expected_x)


@pytest.mark.parametrize(["primitive", "expected_x"], [({}, []), ({"x": [0]}, [0])])
def test_default_factory_field_dataclass_ok(
    primitive: Primitive, expected_x: List[int]
) -> None:
    @dataclass(frozen=True)
    class Data:
        x: List[int] = field(default_factory=list)

    assert terramare.structure(primitive, into=Data) == Data(expected_x)


@pytest.mark.parametrize(
    "primitive",
    [
        {},
        # Data.x is not a constructor parameter so terramare should treat the "x"
        # key as an unknown field. Unknown fields are ignored by default.
        {"x": 1},
    ],
)
def test_init_false_dataclass_ok(primitive: Primitive) -> None:
    @dataclass(frozen=True)
    class Data:
        x: int = field(default=0, init=False)

    data = terramare.structure(primitive, into=Data)
    assert data == Data()
    assert data.x == 0


def test_init_false_dataclass_err() -> None:
    @terramare.disallow_unknown_fields
    @dataclass(frozen=True)
    class Data:
        x: int = field(default=0, init=False)

    # Make sure the class is still constructible with valid input data.
    data = terramare.structure({}, into=Data)
    assert data == Data()
    assert data.x == 0

    with raises(terramare.ConstructorError, assert_contains='unknown field(s): "x"'):
        terramare.structure({"x": 1}, into=Data)


@pytest.mark.parametrize(
    "primitive",
    [
        {},
        # Data.x is not a constructor parameter so terramare should treat the "x"
        # key as an unknown field. Unknown fields are ignored by default.
        {"x": 1},
    ],
)
def test_classvar_dataclass_ok(primitive: Primitive) -> None:
    @dataclass(frozen=True)
    class Data:
        x: ClassVar[int] = 0

    # Data.x is not a constructor parameter so terramare should treat the "x"
    # key as an unknown field. Unknown fields are ignored by default.
    data = terramare.structure(primitive, into=Data)
    assert data == Data()
    assert data.x == 0


def test_initvar_dataclass_ok() -> None:
    @dataclass
    class Data:
        x: InitVar[int]
        y: int = 0

        def __post_init__(self, x: int) -> None:
            self.y += x

    data = terramare.structure({"x": 1, "y": 2}, into=Data)
    assert isinstance(data, Data)
    assert data.y == 3
    assert not hasattr(data, "x")


@pytest.mark.parametrize(
    "defaults",
    [
        pytest.param({}, id=""),
        pytest.param({"defaulted": 1}, id="defaulted"),
        pytest.param({"default_factory": [0]}, id="default_factory"),
        pytest.param(
            {"defaulted": 1, "default_factory": [0]}, id="defaulted,default_factory"
        ),
    ],
)
def test_multi_field_dataclass_ok(defaults: Mapping[str, Any]) -> None:
    initvar_value = None

    @dataclass(frozen=True)
    class Data:
        CLASS_VAR: ClassVar[int] = 0
        initvar: InitVar[int]
        primitive: int
        complex_: Mapping[str, int]
        defaulted: int = 0
        default_factory: List[int] = field(default_factory=list)
        init_false: int = field(default=0, init=False)

        def __post_init__(self, initvar: int) -> None:
            nonlocal initvar_value
            initvar_value = initvar

    data = terramare.structure(
        {
            "CLASS_VAR": 1,
            "initvar": 0,
            "primitive": 0,
            "complex_": {"x": 1},
            "init_false": 1,
            **defaults,
        },
        into=Data,
    )
    assert initvar_value == 0
    # pylint: disable=unexpected-keyword-arg
    assert data == Data(  # type: ignore[call-arg]
        primitive=0,
        complex_={"x": 1},
        defaulted=defaults.get("defaulted", 0),
        default_factory=defaults.get("default_factory", []),
        initvar=0,
    )
    assert data.CLASS_VAR == 0
    assert data.init_false == 0


# We can't define this class inside the test function, because then we can't
# refer to it in the class body.
@dataclass(frozen=True)
class RecursiveData:
    x: Optional["RecursiveData"] = None


def test_recursive_dataclass_ok() -> None:

    assert terramare.structure({"x": {"x": {}}}, into=RecursiveData) == RecursiveData(
        RecursiveData(RecursiveData())
    )


@pytest.mark.py310plus
def test_kw_only_dataclass_ok() -> None:
    # pylint: disable=unexpected-keyword-arg
    @dataclass(frozen=True, kw_only=True)  # type: ignore[call-overload]
    class Data:
        x: int

    assert terramare.structure({"x": 1}, into=Data) == Data(x=1)


@pytest.mark.py310plus
def test_kw_only_dataclass_field_ok() -> None:
    @dataclass(frozen=True)
    class Data:
        # pylint: disable=unexpected-keyword-arg
        x: int = field(kw_only=True)  # type: ignore[call-overload]

    assert terramare.structure({"x": 1}, into=Data) == Data(x=1)


@pytest.mark.py310plus
def test_kw_only_dataclass_fields_ok() -> None:
    # pylint: disable=import-outside-toplevel
    from dataclasses import KW_ONLY  # type: ignore[attr-defined]

    @dataclass(frozen=True)
    class Data:
        _: KW_ONLY
        x: int
        y: int

    # pylint: disable=no-value-for-parameter
    assert terramare.structure({"x": 1, "y": 2}, into=Data) == Data(  # type: ignore[call-arg]
        x=1, y=2
    )


def test_context_initvar_dataclass_ok() -> None:
    @dataclass
    class Data:
        x: Optional[int] = None
        y: InitVar[terramare.Context] = terramare.Context.EMPTY

        def __post_init__(self, context: terramare.Context) -> None:
            if self.x is None:
                x = context["x"]
                assert isinstance(x, int)
                self.x = x

    assert terramare.structure({}, into=Data, context={"x": 1}) == Data(1)
    assert terramare.structure({"x": 1}, into=Data) == Data(1)


def test_fn_optional_positional_only_fields_ok() -> None:
    """Optional positional-only fields should be ignored, even if a value is supplied."""

    def fn(x: int = 0, /, y: int = 1) -> List[int]:
        return [x, y]

    assert terramare.structure({"x": 1, "y": 2}, into=terramare.auto(fn)) == [0, 2]


def test_fn_kwargs_with_disallow_unknown_fields() -> None:
    """
    Unknown fields should be passed via **kwargs if possible.

    This holds even if the function is marked with @disallow_unknown_fields.
    """

    @terramare.disallow_unknown_fields
    @terramare.auto
    def fn(**kwargs: int) -> List[str]:
        return list(kwargs)

    assert terramare.structure({"a": 1}, into=fn) == ["a"]


def test_fn_kwargs_ordering_preserved() -> None:
    """The order of the input fields should be preserved in **kwargs."""

    @terramare.auto
    def fn(_x: int, **kwargs: int) -> List[str]:
        return list(kwargs)

    assert terramare.structure({"a": 1, "b": 2, "_x": 3, "c": 4}, into=fn) == [
        "a",
        "b",
        "c",
    ]


def test_fn_context_parameter_shadows_kwargs() -> None:
    """
    A named Context field takes precedence over a field of the same name destined for **kwargs.

    This is awkward but there's really no alternative. We must pass a value of
    the correct type to the named field, and we can't pass arguments of the same
    name through **kwargs.
    """

    @terramare.auto
    def fn(x: terramare.Context, **kwargs: int) -> List[str]:
        assert isinstance(x, terramare.Context)
        return list(kwargs)

    assert terramare.structure({"x": 1, "y": 2}, into=fn) == ["y"]


@pytest.mark.parametrize(
    "primitive", [{"x": 1, "z": 3}, {"x": 1, "y": 2, "z": 3}], ids=str
)
def test_fn_skip_parameter_ok(primitive: Primitive) -> None:
    @terramare.auto
    @terramare.fields({"y": terramare.skip})
    def fn(x: int, y: int = 0, **kwargs: int) -> Dict[str, int]:
        return {"x": x, "y": y, **kwargs}

    assert terramare.structure(primitive, into=fn) == {"x": 1, "y": 0, "z": 3}


def test_fn_skip_positional_only_parameter_ok() -> None:
    def fn(
        x: terramare.Context,
        y: int = 0,
        z: terramare.Context = terramare.Context.EMPTY,
        /,
    ) -> Any:
        return (x, y, z)

    fn = terramare.fields({"y": terramare.skip})(fn)
    fn = terramare.auto(fn)
    assert terramare.structure({"x": 1, "y": 2, "z": 3}, into=fn) == (
        terramare.Context.EMPTY,
        0,
        terramare.Context.EMPTY,
    )


def test_fn_skip_required_parameter_err() -> None:
    @terramare.auto
    @terramare.fields({"_y": terramare.skip})
    def fn(_x: int, _y: int) -> None:
        pass

    with raises(
        terramare.FactoryError, assert_contains="cannot skip required parameter: _y"
    ):
        terramare.structure({}, into=fn)


def test_fn_from_context_required_ok() -> None:
    @terramare.auto
    @terramare.fields({"x": terramare.metadata.from_context("int")})
    def fn(x: int) -> int:
        return x

    assert terramare.structure({}, into=fn, context={"int": 1}) == 1


def test_fn_from_context_optional_provided_ok() -> None:
    @terramare.auto
    @terramare.fields({"x": terramare.metadata.from_context("int")})
    def fn(x: int = 0) -> int:
        return x

    assert terramare.structure({}, into=fn, context={"int": 1}) == 1


def test_fn_from_context_optional_missing_ok() -> None:
    @terramare.auto
    @terramare.fields({"x": terramare.metadata.from_context("int")})
    def fn(x: int = 0) -> int:
        return x

    assert terramare.structure({}, into=fn, context={}) == 0


def test_fn_from_context_positional_only_ok() -> None:
    def fn(x: int, /) -> int:
        return x

    fn = terramare.fields({"x": terramare.metadata.from_context("int")})(fn)
    fn = terramare.auto(fn)
    assert terramare.structure({}, into=fn, context={"int": 1}) == 1
