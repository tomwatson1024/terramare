"""Tests for constructing Enum types."""

import enum
from typing import Any, List

import pytest

from terramare import enums, terramare
from terramare.errors import ConstructorError, FactoryError, UnsupportedTargetTypeError
from terramare.types import Primitive

from . import test_utils
from .test_utils import raises


class _BasicEnum(enum.Enum):
    VALUE_1 = 1
    VALUE_2 = 2


class _HetrogeneousEnum(enum.Enum):
    VALUE_1 = 1
    VALUE_2 = "2"


@pytest.mark.parametrize(
    ["enum_t", "input_value", "expected_output_value"],
    [
        pytest.param(_BasicEnum, 1, _BasicEnum.VALUE_1),
        pytest.param(_BasicEnum, 2, _BasicEnum.VALUE_2),
        pytest.param(_HetrogeneousEnum, 1, _HetrogeneousEnum.VALUE_1),
        pytest.param(_HetrogeneousEnum, "2", _HetrogeneousEnum.VALUE_2),
    ],
)
def test_construct_enum_mainline(
    enum_t: type, input_value: Primitive, expected_output_value: Any
) -> None:
    """Test that an enum value can be constructed."""
    assert terramare.structure(input_value, into=enum_t) == expected_output_value


@pytest.mark.parametrize(
    ["enum_t", "input_value", "error_contains"],
    [
        pytest.param(_BasicEnum, 3, ["1", "2", "3"], id="unknown value"),
        pytest.param(_HetrogeneousEnum, "3", ["1", "2", "3"], id="type mismatch"),
    ],
)
def test_construct_enum_error(
    enum_t: type, input_value: Primitive, error_contains: List[str]
) -> None:
    """Test error cases when constructing an enum."""
    with raises(ConstructorError, assert_contains=error_contains):
        terramare.structure(input_value, into=enum_t)


@pytest.mark.parametrize(
    ["non_enum_t"],
    [
        pytest.param(0),
        pytest.param(int),
        pytest.param(List),
        pytest.param(Any, id="Any"),
    ],
)
def test_not_an_enum_error(non_enum_t: Any) -> None:
    """Test creating an enum constructor from a non-enum."""
    with pytest.raises(UnsupportedTargetTypeError):
        test_utils.create_constructor_internal(enums.EnumFactory(), non_enum_t)


class _NonDeserializableEnum(enum.Enum):
    VALUE_1 = 1
    VALUE_2 = {"key": "value"}


def test_non_constructible_value_type() -> None:
    """Test an enum with a non-constructible member type."""
    with raises(FactoryError, assert_contains="only enum values"):
        terramare.structure({}, into=_NonDeserializableEnum)
