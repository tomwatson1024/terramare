"""Tests for constructing tagged types."""

from dataclasses import dataclass
from typing import List

import pytest

from terramare import externally_tagged, internally_tagged, terramare, types
from terramare.errors import ConstructorError, FactoryError

from . import test_utils
from .test_utils import raises


@internally_tagged(
    key="type",
    variants=lambda: {"int": _InternallyTaggedInt, "str": _InternallyTaggedStr},
)
class _InternallyTagged:
    pass


@internally_tagged(
    key="type",
    variants=lambda: {"int": _InternallyTaggedInt, "str": _InternallyTaggedStr},
    default_variant="int",
)
class _InternallyTaggedDefault:
    pass


@dataclass(frozen=True)
class _InternallyTaggedInt(_InternallyTagged):
    integer: int


@dataclass(frozen=True)
class _InternallyTaggedStr(_InternallyTagged):
    string: str


@externally_tagged(lambda: {"int": _ExternallyTaggedInt, "str": _ExternallyTaggedStr})
class _ExternallyTagged:
    pass


@dataclass(frozen=True)
class _ExternallyTaggedInt(_ExternallyTagged):
    integer: int


@dataclass(frozen=True)
class _ExternallyTaggedStr(_ExternallyTagged):
    string: str


def test_internally_tagged_mainline() -> None:
    """Test mainline construction of an internally-tagged type."""
    assert terramare.structure(
        {"type": "int", "integer": 0}, into=_InternallyTagged
    ) == _InternallyTaggedInt(integer=0)
    assert terramare.structure(
        {"type": "str", "string": "string"}, into=_InternallyTagged
    ) == _InternallyTaggedStr(string="string")


def test_internally_tagged_default() -> None:
    """Test an internally-tagged type with a default variant."""
    assert terramare.structure(
        {"type": "int", "integer": 0}, into=_InternallyTagged
    ) == _InternallyTaggedInt(integer=0)
    assert terramare.structure(
        {"type": "str", "string": "string"}, into=_InternallyTagged
    ) == _InternallyTaggedStr(string="string")
    assert terramare.structure(
        {"integer": 0}, into=_InternallyTaggedDefault
    ) == _InternallyTaggedInt(integer=0)


def test_externally_tagged_mainline() -> None:
    """Test mainline construction of an externally-tagged type."""
    assert terramare.structure(
        {"int": {"integer": 0}}, into=_ExternallyTagged
    ) == _ExternallyTaggedInt(integer=0)
    assert terramare.structure(
        {"str": {"string": "string"}}, into=_ExternallyTagged
    ) == _ExternallyTaggedStr(string="string")


@pytest.mark.parametrize("type_", [_InternallyTagged, _InternallyTaggedDefault])
@pytest.mark.parametrize(
    ["value", "error_contains"],
    [
        pytest.param([], [], id="not a dict"),
        pytest.param({}, [], id="missing key"),
        pytest.param({"type": "bool"}, [], id="key value mismatch"),
        pytest.param({"type": "str"}, [], id="bad remainder"),
    ],
)
def test_internally_tagged_errors(
    type_: type, value: types.Primitive, error_contains: List[str]
) -> None:
    """Test errors when constructing an internally-tagged type."""
    with raises(ConstructorError, assert_contains=error_contains):
        terramare.structure(value, into=type_)


@pytest.mark.parametrize(
    ["value", "error_contains"],
    [
        pytest.param([], [], id="not a dict"),
        pytest.param({}, [], id="missing key"),
        pytest.param(
            {"int": {"integer": 0}, "str": {"string": "string"}}, [], id="multiple keys"
        ),
        pytest.param({"bool": {}}, [], id="key value mismatch"),
        pytest.param({"str": {"integer": 0}}, [], id="bad value"),
    ],
)
def test_externally_tagged_errors(
    value: types.Primitive, error_contains: List[str]
) -> None:
    """Test errors when constructing an externally-tagged type."""
    with raises(ConstructorError, assert_contains=error_contains):
        terramare.structure(value, into=_ExternallyTagged)


def test_internally_tagged_bad_variant() -> None:
    """Test the error raised when a variant is not constructible."""

    @internally_tagged(
        "type", lambda: {"nonconstructible": test_utils.NonConstructible}
    )
    class _BadValue:
        pass

    with raises(FactoryError, assert_contains="NonConstructible"):
        terramare.structure({}, into=_BadValue)


def test_internally_tagged_bad_default_variant() -> None:
    """Test the error raised when the default variant does not exist."""

    @internally_tagged("type", lambda: {"int": int}, default_variant="missing")
    class _BadDefault:
        pass

    with raises(FactoryError, assert_contains="unknown default variant"):
        terramare.structure({}, into=_BadDefault)
