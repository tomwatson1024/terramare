"""Sphinx configuration."""

import os
import sys

from terramare import __version__

sys.path.insert(0, os.path.abspath("../src"))

project = "terramare"
release = __version__
version = __version__
copyright = "2020"  # pylint: disable=redefined-builtin
author = "Tom W"

exclude_patterns = ["_build", "_env", ".mypy_cache"]
extensions = [
    "sphinx.ext.autodoc",
    "sphinx_copybutton",
]
html_static_path = ["_static"]
templates_path = ["_templates"]

html_theme = "furo"
pygments_style = "onehalf-light"
pygments_dark_style = "onehalf-dark"

autosectionlabel_prefix_document = True
autodoc_default_options = {"inherited-members": False, "member-order": "bysource"}
copybutton_prompt_text = ">>> "
