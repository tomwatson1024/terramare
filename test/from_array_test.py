"""Tests for constructing class/function types from arrays."""

from typing import List, Sequence, Tuple

import pytest

import terramare
from terramare import ARRAY, auto
from terramare.types import Primitive

from .test_utils import raises


def test_fn_optional_keyword_only_fields_ok() -> None:
    """Optional keyword-only fields should be ignored, even if a value is supplied."""

    @auto(from_=ARRAY)
    def fn(x: int, *, y: int = 0) -> Tuple[int, int]:
        return (x, y)

    assert terramare.structure([0, 1], into=fn) == (0, 0)


@pytest.mark.parametrize(
    ["primitive", "expected_output"],
    [([1], [1, 0, 0]), ([1, 3], [1, 0, 3])],
    ids=str,
)
def test_fn_skip_parameter_ok(
    primitive: Primitive, expected_output: Sequence[int]
) -> None:
    @terramare.auto(from_=ARRAY)
    @terramare.fields({"y": terramare.skip})
    def fn(x: int, y: int = 0, z: int = 0) -> List[int]:
        return [x, y, z]

    assert terramare.structure(primitive, into=fn) == expected_output


def test_fn_skip_keyword_only_parameter_ok() -> None:
    @terramare.auto(from_=ARRAY)
    @terramare.fields({"y": terramare.skip})
    def fn(
        x: int, *, y: terramare.Context = terramare.Context.EMPTY
    ) -> Tuple[int, terramare.Context]:
        return (x, y)

    assert terramare.structure([1], into=fn, context={"hello": "world"}) == (
        1,
        terramare.Context.EMPTY,
    )


def test_fn_skip_required_parameter_err() -> None:
    @terramare.auto(from_=ARRAY)
    @terramare.fields({"_y": terramare.skip})
    def fn(_x: int, _y: int) -> None:
        pass

    with raises(
        terramare.FactoryError, assert_contains="cannot skip required parameter: _y"
    ):
        terramare.structure({}, into=fn)


def test_fn_from_context_required_ok() -> None:
    @terramare.auto(from_=ARRAY)
    @terramare.fields({"x": terramare.metadata.from_context("int")})
    def fn(x: int) -> int:
        return x

    assert terramare.structure([], into=fn, context={"int": 1}) == 1


def test_fn_from_context_optional_provided_ok() -> None:
    @terramare.auto(from_=ARRAY)
    @terramare.fields({"x": terramare.metadata.from_context("int")})
    def fn(x: int = 0) -> int:
        return x

    assert terramare.structure([], into=fn, context={"int": 1}) == 1


def test_fn_from_context_optional_missing_ok() -> None:
    @terramare.auto(from_=ARRAY)
    @terramare.fields({"x": terramare.metadata.from_context("int")})
    def fn(x: int = 0) -> int:
        return x

    assert terramare.structure([], into=fn, context={}) == 0


def test_fn_from_context_keyword_only_ok() -> None:
    @terramare.fields({"x": terramare.metadata.from_context("int")})
    @terramare.auto(from_=ARRAY)
    def fn(*, x: int = 0) -> int:
        return x

    assert terramare.structure([], into=fn, context={"int": 1}) == 1
