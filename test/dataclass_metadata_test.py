"""Tests for dataclass field metadata."""

from dataclasses import dataclass, field
from typing import Any, Mapping

import pytest

import terramare
from terramare.metadata import MetadataError

from .test_utils import raises


@pytest.mark.parametrize(
    "metadata",
    [
        pytest.param(terramare.skip, id="single metadata"),
        pytest.param(
            {terramare.METADATA_KEY: terramare.skip}, id="single terramare metadata"
        ),
        pytest.param(
            {terramare.METADATA_KEY: [terramare.skip]},
            id="map of terramare metadata",
        ),
    ],
)
def test_dataclass_metadata(metadata: Mapping[str, Any]) -> None:
    @dataclass(frozen=True)
    class A:
        a: int = field(default=1, metadata=metadata)

    assert terramare.structure({"a": 2}, into=A) == A(1)


def test_no_dataclass_field_for_init_parameter() -> None:
    """Test a dataclass with an __init__ parameter with no corresponding Field."""

    @dataclass
    class A:
        a: int = field(metadata=terramare.skip)

        def __init__(self, b: int) -> None:
            self.a = b

    assert terramare.structure({"b": 2}, into=A) == A(2)


@pytest.mark.parametrize(
    "bad_metadata",
    [pytest.param("oh no!", id="str"), pytest.param(["oh no!"], id="List[str]")],
)
def test_bad_field_metadata_type(bad_metadata: Any) -> None:
    @dataclass
    class A:
        a: int = field(metadata={terramare.METADATA_KEY: bad_metadata})

    with raises(MetadataError):
        terramare.structure({}, into=A)
