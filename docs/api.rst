API Reference
=============

.. warning::

   The following is the complete public API of ``terramare`` for the purposes of semantic versioning.
   Any part of the package not documented here may be changed without warning

.. role:: python(code)
   :language: python

.. autofunction:: terramare.structure

Errors
------

.. autoexception:: terramare.ConstructorError()

.. autoexception:: terramare.FactoryError()

.. autoexception:: terramare.TerramareError()

Decorators
----------

.. autoclass:: terramare.auto

.. autoclass:: terramare.disallow_unknown_fields

>>> import terramare
>>> from dataclasses import dataclass
>>>
>>> @terramare.disallow_unknown_fields
... @dataclass
... class User:
...     id: int
...     name: str
>>>
>>> terramare.structure({"id": 1, "name": "Alice", "age": 27}, into=User)
Traceback (most recent call last):
...
terramare.errors.ConstructorError: .: unknown field(s): "age"
...

.. autoclass:: terramare.externally_tagged

.. autoclass:: terramare.handle_exception_types

.. autoclass:: terramare.internally_tagged

.. autoclass:: terramare.with_
