"""Tests for constructing primitive types."""

from typing import Any, Type

import pytest

from terramare import terramare
from terramare.errors import ConstructorError
from terramare.types import Primitive

from .test_utils import raises


@pytest.mark.parametrize(
    ["primitive_t", "input_value"],
    [
        pytest.param(Any, "string", id="string as Any"),
        pytest.param(Any, {"key": "value"}, id="dict as Any"),
        pytest.param(object, "string", id="string as object"),
        pytest.param(object, {"key": "value"}, id="dict as object"),
        pytest.param(str, "string", id="string"),
        pytest.param(str, "1", id="int string"),
        pytest.param(int, 1, id="1 as int"),
        pytest.param(int, 0, id="0 as int"),
        pytest.param(int, -1, id="-1 as int"),
        pytest.param(float, 1.0, id="float"),
        pytest.param(float, 1, id="int as float"),
        pytest.param(dict, {}, id="empty dict"),
        pytest.param(dict, {"a": 0}, id="homogeneous dict"),
        pytest.param(dict, {"a": 0, "b": "c"}, id="hetrogeneous dict"),
        pytest.param(list, [], id="empty list"),
        pytest.param(list, ["a"], id="homogeneous list"),
        pytest.param(list, [0, "a"], id="hetrogeneous list"),
        pytest.param(bool, True, id="True"),
        pytest.param(bool, False, id="False"),
        pytest.param(type(None), None, id="None"),
    ],
)
def test_construct_primitive_mainline(
    primitive_t: Type[Primitive], input_value: Primitive
) -> None:
    """
    Test the primitive constructors.

    When the input value is of the correct type, it should be returned unchanged.
    """
    assert terramare.structure(input_value, into=primitive_t) == input_value


@pytest.mark.parametrize(
    ["primitive_t", "input_value", "expected_output_value"],
    [
        pytest.param(int, "1", 1, id="string as int"),
        pytest.param(float, "1.23", 1.23, id="string as float"),
        pytest.param(bool, "true", True, id="string as bool (True)"),
        pytest.param(bool, "false", False, id="string as bool (False)"),
    ],
)
def test_construct_primitive_coerce_strings_mainline(
    primitive_t: Type[Primitive],
    input_value: str,
    expected_output_value: Primitive,
) -> None:
    """Test the primitive constructors with the coerce_strings option enabled."""
    assert (
        terramare.structure(input_value, into=primitive_t, coerce_strings=True)
        == expected_output_value
    )


@pytest.mark.parametrize(
    ["primitive_t", "input_value", "error_contains"],
    [
        pytest.param(str, 1, "string", id="int as string"),
        pytest.param(str, None, "string", id="None as string"),
        pytest.param(int, False, "integer", id="False as int"),
        pytest.param(int, "1", "integer", id="string as int"),
        pytest.param(int, None, "integer", id="None as int"),
        pytest.param(bool, 0, "bool", id="int as bool"),
        pytest.param(bool, None, "bool", id="None as bool"),
        pytest.param(type(None), 0, "null", id="int as NoneType"),
        pytest.param(float, "1", "number", id="str as float"),
        pytest.param(float, None, "number", id="None as float"),
    ],
)
def test_construct_primitive_error(
    primitive_t: Type[Primitive],
    input_value: Primitive,
    error_contains: str,
) -> None:
    """
    Test the primitive constructors' error cases.

    When the input value is not of the correct type an exception should be
    raised that explains the type mismatch.
    """
    with raises(ConstructorError, assert_contains=error_contains):
        terramare.structure(input_value, into=primitive_t)


@pytest.mark.parametrize(
    ["primitive_t", "input_value", "error_contains"],
    [
        pytest.param(
            int, 1, "(string representation of) integer", id="non-string as int"
        ),
        pytest.param(
            int,
            "1.0",
            "(string representation of) integer",
            id="invalid string as int",
        ),
        pytest.param(
            float, 1.0, "(string representation of) number", id="non-string as float"
        ),
        pytest.param(
            float,
            "notaflt",
            "(string representation of) number",
            id="invalid string as float",
        ),
        pytest.param(
            bool, True, "(string representation of) bool", id="non-string as bool"
        ),
        pytest.param(
            bool,
            "notabl",
            "(string representation of) bool",
            id="invalid string as bool",
        ),
        pytest.param(int, ["1"], "integer", id="list as int"),
        pytest.param(int, {"a": "1"}, "integer", id="dict as int"),
    ],
)
def test_construct_primitive_coerce_strings_error(
    primitive_t: Type[Primitive],
    input_value: Primitive,
    error_contains: str,
) -> None:
    """Test the primitive constructors' error cases with coerce_strings."""
    with raises(ConstructorError, assert_contains=error_contains):
        terramare.structure(input_value, into=primitive_t, coerce_strings=True)
