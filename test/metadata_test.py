"""Tests for terramare.metadata module."""

from dataclasses import dataclass
from typing import ClassVar

import pytest

from terramare import metadata


@dataclass(frozen=True)
class test_metadata_int(metadata.Metadata[int]):
    KEY: ClassVar[str] = f"{__name__}.test_metadata_int"
    DEFAULT: ClassVar[int] = 0

    _value: int

    @property
    def value(self) -> int:
        return self._value


@dataclass(frozen=True)
class test_metadata_str(metadata.Metadata[str]):
    KEY: ClassVar[str] = f"{__name__}.test_metadata_str"
    DEFAULT: ClassVar[str] = ""

    _value: str

    @property
    def value(self) -> str:
        return self._value


def test_metadata_on_type() -> None:
    @test_metadata_int(1)
    class MyType:
        pass

    collection = metadata.MetadataCollection.new({MyType: [test_metadata_str("x")]})
    assert test_metadata_int.read(collection, MyType) == 1


def test_metadata_in_collection() -> None:
    class MyType:
        pass

    collection = metadata.MetadataCollection.new({MyType: [test_metadata_str("x")]})
    assert test_metadata_str.read(collection, MyType) == "x"


def test_bad_metadata_type() -> None:
    class MyType:
        pass

    setattr(MyType, metadata._METADATA_FIELD, 0)  # pylint: disable=protected-access

    with pytest.raises(metadata.MetadataError):
        metadata.MetadataCollection().read(MyType, "meta", 0)


@dataclass(frozen=True)
class DummyFieldMetadata(metadata.FieldMetadata[int]):
    KEY: ClassVar[str] = "test"
    DEFAULT: ClassVar[int] = 0
    _value: int

    @property
    def value(self) -> int:
        return self._value


def test_field_metadata_iter() -> None:
    """
    Test the __iter__ implementation of DummyFieldMetadata.

    Field metadata should act as a Mapping of METADATA_KEY to itself, for
    ergonomic use in dataclassses.fields metadata kwarg.
    """
    assert list(DummyFieldMetadata(1)) == [metadata.METADATA_KEY]


def test_field_metadata_len() -> None:
    """Test the __len__ implementation of DummyFieldMetadata, as above."""
    assert len(DummyFieldMetadata(1)) == 1
