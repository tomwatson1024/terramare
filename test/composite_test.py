"""Tests for terramare.composite module."""

from terramare import FactoryError, composite

from .test_utils import create_constructor_internal, raises


def test_composite_error() -> None:
    with raises(FactoryError, assert_contains="unable to construct type"):
        create_constructor_internal(composite.CompositeFactory(), int)
