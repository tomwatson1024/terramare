"""Tests for `with_` metadata."""

from dataclasses import dataclass, field

import terramare


@terramare.auto
def add(x: int, y: int) -> int:
    return x + y


def test_with_field_from_object() -> None:
    @dataclass(frozen=True)
    class Data:
        added: int = field(metadata=terramare.with_(lambda: add))

    assert terramare.structure({"added": {"x": 1, "y": 2}}, into=Data) == Data(3)


def test_with_field_from_array() -> None:
    @terramare.auto(from_=terramare.ARRAY)
    @dataclass(frozen=True)
    class Data:
        added: int = field(metadata=terramare.with_(lambda: add))

    assert terramare.structure([{"x": 1, "y": 2}], into=Data) == Data(3)
