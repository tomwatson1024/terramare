"""Tests for terramare.stdlib_metadata module."""

import datetime
import ipaddress
import pathlib
import re
from typing import Any, Pattern

import pytest

import terramare
from terramare.errors import ConstructorError
from terramare.types import Target

from .test_utils import raises


def test_datetime_mainline() -> None:
    """Test constructing a valid datetime."""
    assert terramare.structure(
        "2020-01-01T00:00:00", into=datetime.datetime
    ) == datetime.datetime(2020, 1, 1)


def test_datetime_error() -> None:
    """Test constructing an invalid datetime."""
    with raises(ConstructorError, assert_contains="abcdef"):
        terramare.structure("abcdef", into=datetime.datetime)


@pytest.mark.parametrize(
    "target",
    [
        pytest.param(Pattern, id="Pattern"),
        pytest.param(Pattern[str], id="Pattern[str]"),
    ],
)
def test_regex_mainline(target: Target[Any]) -> None:
    """Test constructing a valid regex."""
    assert terramare.structure(".*", into=target) == re.compile(".*")


@pytest.mark.parametrize(
    "target",
    [
        pytest.param(Pattern, id="Pattern"),
        pytest.param(Pattern[str], id="Pattern[str]"),
    ],
)
def test_regex_error(target: Target[Any]) -> None:
    """Test constructing an invalid regex."""
    with raises(ConstructorError):
        terramare.structure("*", into=target)


def test_ipv4_address_mainline() -> None:
    """Test constructing a valid IPv4 address."""
    assert terramare.structure(
        "0.0.0.0", into=ipaddress.IPv4Address
    ) == ipaddress.IPv4Address("0.0.0.0")


def test_ipv4_address_error() -> None:
    """Test constructing an invalid IPv4 address."""
    with raises(ConstructorError, assert_contains="oh no"):
        terramare.structure("oh no", into=ipaddress.IPv4Address)


def test_ipv6_address_mainline() -> None:
    """Test constructing a valid IPv6 address."""
    assert terramare.structure(
        "::", into=ipaddress.IPv6Address
    ) == ipaddress.IPv6Address("::")


def test_ipv6_address_error() -> None:
    """Test constructing an invalid IPv6 address."""
    with raises(ConstructorError, assert_contains="oh no"):
        terramare.structure("oh no", into=ipaddress.IPv6Address)


def test_pathlib_path() -> None:
    """Test constructing a path."""
    assert terramare.structure("/home", into=pathlib.Path) == pathlib.Path("/home")
