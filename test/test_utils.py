"""Common test utilities."""

import contextlib
from typing import (
    Any,
    Callable,
    Iterator,
    NamedTuple,
    Optional,
    Sequence,
    Type,
    TypeVar,
    Union,
    overload,
)

import pytest

from terramare.core import (
    Constructor,
    Factory,
    FactoryCore,
    InternalConstructor,
    InternalFactory,
    InternalFactoryError,
    _Stack,
)
from terramare.errors import FactoryError
from terramare.pretty_printer import print_type_name
from terramare.types import Target


class NonConstructible(NamedTuple):
    """A class that is not constructible."""

    fn: Callable[..., None]


_T_co = TypeVar("_T_co", covariant=True)


@overload
def create_constructor_internal(
    factory: FactoryCore,
    target: Target[_T_co],
) -> Constructor[_T_co]:
    # See terramare.py for an explanation of the overloads here.
    ...  # pragma: no cover


@overload
def create_constructor_internal(
    factory: FactoryCore,
    target: Any,
) -> Constructor[Any]: ...  # pragma: no cover


def create_constructor_internal(
    factory: FactoryCore,
    target: Any,
) -> Constructor[Any]:
    try:
        return Constructor(
            InternalConstructor(
                factory.create_constructor(
                    create_internal_constructor_factory(),
                    target,
                ),
                target,
                _Stack(),
            ),
        )
    except InternalFactoryError as e:
        raise FactoryError(
            f"failed to create constructor for type '{print_type_name(target)}': "
            f"{e.summary}\n{e.detail}"
        ) from e


def create_internal_constructor_factory() -> InternalFactory:
    # pylint: disable=protected-access
    return InternalFactory(Factory.new()._persistent_data)


@contextlib.contextmanager
def raises(
    exc: Type[Exception], *, assert_contains: Optional[Union[str, Sequence[str]]] = None
) -> Iterator[None]:
    if assert_contains is None:
        assert_contains = []
    elif isinstance(assert_contains, str):
        assert_contains = [assert_contains]

    with pytest.raises(exc) as excinfo:
        yield
    # Include a leading newline to separate output from test name in pytest.
    print(f"\n{excinfo.value}")  # noqa: T201

    for s in assert_contains:
        assert s in str(excinfo.value)
