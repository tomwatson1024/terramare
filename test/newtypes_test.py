"""Tests for constructing NewType types."""

from dataclasses import dataclass
from typing import Any, NewType

import pytest

from terramare import newtypes, terramare
from terramare.errors import ConstructorError, FactoryError, UnsupportedTargetTypeError
from terramare.types import Primitive

from . import test_utils
from .test_utils import raises


@dataclass(frozen=True)
class _Class:
    i: int


@pytest.mark.parametrize(
    ["newtype_t", "input_value", "expected_value"],
    [
        pytest.param(NewType("s", str), "string", "string", id="string alias"),
        pytest.param(NewType("i", int), 1, 1, id="int alias"),
    ],
)
def test_construct_newtype_mainline(
    newtype_t: type, input_value: Primitive, expected_value: Any
) -> None:
    """Test that a newtype can be constructed."""
    assert terramare.structure(input_value, into=newtype_t) == expected_value


def test_construct_class_newtype() -> None:
    """Test that a newtype class can be constructed."""
    assert terramare.structure({"i": 1}, into=NewType("c", _Class)) == _Class(1)


@pytest.mark.parametrize(
    ["newtype_t", "input_value", "error_contains"],
    [pytest.param(NewType("s", str), 1, "expected str", id="type mismatch")],
)
def test_construct_newtype_error(
    newtype_t: type, input_value: Primitive, error_contains: str
) -> None:
    """Test error cases when constructing a newtype."""
    with raises(ConstructorError, assert_contains=error_contains):
        terramare.structure(input_value, into=newtype_t)


def test_not_a_newtype_error() -> None:
    """Test creating a newtype constructor from a non-newtype."""
    with pytest.raises(UnsupportedTargetTypeError):
        test_utils.create_constructor_internal(newtypes.NewTypeFactory(), int)


def test_non_constructible_parameter_type() -> None:
    """Test a newtype alias for a non-constructible type."""
    with raises(FactoryError, assert_contains="NonConstructible"):
        terramare.structure(
            {},
            into=NewType("NewNonConstructible", test_utils.NonConstructible),
        )
