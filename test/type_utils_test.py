"""Tests for terramare.type_utils module."""

from typing import Any, Dict, Sequence

import pytest
from typing_extensions import Literal

from terramare import errors, type_utils
from terramare.types import Target


@pytest.mark.parametrize(
    ["type_", "generic"],
    [
        pytest.param(Dict, Dict, id="Dict"),
        pytest.param(Dict[str, Any], Dict, id="Dict[str, Any]"),
    ],
)
def test_get_base_of_generic_type(type_: Target[Any], generic: Any) -> None:
    """Test determining the generic type of which type_ is an instance."""
    assert type_utils.get_base_of_generic_type(type_) == generic


@pytest.mark.parametrize(
    ["from_type", "expected_params"],
    [
        pytest.param(Dict, [str, Any], id="Dict"),
        pytest.param(Dict[str, Any], [str, Any], id="Dict[str, Any]"),
        pytest.param(Literal[1], [1], id="Literal[1]"),
        pytest.param(Literal[True], [True], id="Literal[True]"),
        pytest.param(int, [], id="int"),
    ],
)
def test_get_type_parameters(
    from_type: Target[Any], expected_params: Sequence[Any]
) -> None:
    """Test extracting the type parameters from a parametrized type."""
    assert type_utils.get_type_parameters(from_type) == expected_params


def test_required_parameter_default_err() -> None:
    def fn(x: int) -> int:
        return x

    parameter = type_utils.get_parameters(fn)[0]

    with pytest.raises(
        errors.InternalError,
        match="attempted to retrieve default of required parameter x",
    ):
        _ = parameter.default
