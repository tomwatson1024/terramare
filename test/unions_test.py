"""Tests for constructing Union types."""

from typing import List, Union

import pytest
from attr import dataclass

from terramare import terramare, unions
from terramare.errors import ConstructorError, FactoryError, UnsupportedTargetTypeError
from terramare.types import Primitive

from . import test_utils
from .test_utils import raises


@pytest.mark.parametrize(
    ["union_t", "input_value"],
    [
        pytest.param(Union[str], "string", id="single type"),
        pytest.param(Union[str, int], "string", id="first of two types"),
        pytest.param(Union[str, int], 1, id="second of two types"),
        pytest.param(Union[bool], True, id="Union[bool]"),
    ],
)
def test_construct_union_mainline(union_t: type, input_value: Primitive) -> None:
    """Test that a union of multiple types can be constructed."""
    assert terramare.structure(input_value, into=union_t) == input_value  # type: ignore


@pytest.mark.parametrize(
    ["union_t", "input_value", "error_contains"],
    [pytest.param(Union[str, bool], 1, ["str", "bool"], id="type mismatch")],
)
def test_construct_union_error(
    union_t: type, input_value: Primitive, error_contains: List[str]
) -> None:
    """Test that an appropriate error is raised if a union cannot be constructed."""
    with raises(ConstructorError, assert_contains=error_contains):
        terramare.structure(input_value, into=union_t)


def test_not_a_union_error() -> None:
    """Test that an error is raised when creating a union constructor from a non-union type."""
    with pytest.raises(UnsupportedTargetTypeError):
        test_utils.create_constructor_internal(unions.UnionFactory(), List[int])


def test_non_constructible_element_type() -> None:
    """Test that an error is raised if element types are not constructible."""
    with raises(FactoryError, assert_contains="NonConstructible"):
        terramare.structure({}, into=Union[test_utils.NonConstructible])


@dataclass(frozen=True)
class Variant1:
    x: int
    y: int


@dataclass(frozen=True)
class Variant2:
    x: int
    z: int


def test_overlapping_fields() -> None:
    assert terramare.structure(
        {"x": 1, "z": 2}, into=Union[Variant1, Variant2]
    ) == Variant2(x=1, z=2)
