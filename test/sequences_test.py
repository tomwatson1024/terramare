"""Tests for constructing sequence types."""

from typing import (
    Any,
    FrozenSet,
    Iterable,
    Iterator,
    List,
    MutableSequence,
    Sequence,
    Set,
    Tuple,
)

import pytest

from terramare import sequences, terramare
from terramare.errors import ConstructorError, FactoryError, UnsupportedTargetTypeError
from terramare.types import Primitive

from . import test_utils
from .test_utils import raises


@pytest.mark.parametrize(
    ["sequence_t", "input_value", "expected_output_value"],
    [
        pytest.param(List[str], [], [], id="empty list"),
        pytest.param(List[str], ["a"], ["a"], id="mainline list"),
        pytest.param(List, ["a"], ["a"], id="unspecified element type list"),
        pytest.param(Sequence[str], [], [], id="empty sequence"),
        pytest.param(Sequence[str], ["a"], ["a"], id="mainline sequence"),
        pytest.param(Sequence, ["a"], ["a"], id="unspecified element type sequence"),
        pytest.param(MutableSequence[str], [], [], id="empty mutable sequence"),
        pytest.param(
            MutableSequence[str], ["a"], ["a"], id="mainline mutable sequence"
        ),
        pytest.param(
            MutableSequence,
            ["a"],
            ["a"],
            id="unspecified element type mutable sequence",
        ),
        pytest.param(Set[str], [], set(), id="empty set"),
        pytest.param(Set[str], ["a"], {"a"}, id="mainline set"),
        pytest.param(Set, ["a"], {"a"}, id="unspecified element type set"),
        pytest.param(FrozenSet[str], [], set(), id="empty frozenset"),
        pytest.param(FrozenSet[str], ["a"], {"a"}, id="mainline frozenset"),
        pytest.param(FrozenSet, ["a"], {"a"}, id="unspecified element type frozenset"),
        pytest.param(Iterable[str], [], [], id="empty iterable"),
        pytest.param(Iterable[str], ["a"], ["a"], id="mainline iterable"),
        pytest.param(Iterable, ["a"], ["a"], id="unspecified element type iterable"),
    ],
)
def test_construct_sequence_mainline(
    sequence_t: type, input_value: Primitive, expected_output_value: Any
) -> None:
    """Test that a (typed) homogeneous sequence can be constructed."""
    assert terramare.structure(input_value, into=sequence_t) == expected_output_value


@pytest.mark.parametrize(
    ["iterator_t", "input_value", "expected_output_value"],
    [
        pytest.param(Iterator[str], [], [], id="empty iterator"),
        pytest.param(Iterator[str], ["a"], ["a"], id="mainline iterator"),
        pytest.param(Iterator, ["a"], ["a"], id="unspecified element type iterator"),
    ],
)
def test_construct_iterator_mainline(
    iterator_t: type, input_value: Primitive, expected_output_value: Any
) -> None:
    """Test that a (typed) iterator can be constructed."""
    it: Iterator[Any] = terramare.structure(input_value, into=iterator_t)
    assert hasattr(it, "__next__")
    assert hasattr(it, "__iter__")
    assert list(it) == expected_output_value


@pytest.mark.parametrize(
    "type_constructor",
    [List, Sequence, MutableSequence, Set, FrozenSet, Iterable, Iterator],
)
@pytest.mark.parametrize(
    ["input_value", "error_contains"],
    [
        pytest.param(0, "expected array", id="not an array"),
        pytest.param([0], "expected string", id="first element type mismatch"),
        pytest.param(["a", 0], "expected string", id="latter element type mismatch"),
    ],
)
def test_construct_sequence_error(
    type_constructor: Any, input_value: Primitive, error_contains: str
) -> None:
    """Test that a descriptive error is raised if a list cannot be constructed."""
    with raises(ConstructorError, assert_contains=error_contains):
        terramare.structure(input_value, into=type_constructor[str])


@pytest.mark.parametrize(
    ["non_sequence_t"],
    [
        pytest.param(Tuple[int, int], id="typed tuple"),
        pytest.param(tuple, id="untyped tuple"),
    ],
)
def test_not_a_list_error(non_sequence_t: type) -> None:
    """Test creating a sequence constructor from a non-sequence type."""
    with pytest.raises(UnsupportedTargetTypeError):
        test_utils.create_constructor_internal(
            sequences.SequenceFactory(), non_sequence_t
        )


def test_non_constructible_element_type() -> None:
    """Test that an error is raised if the element type is not constructible."""
    with raises(FactoryError, assert_contains="NonConstructible"):
        terramare.structure({}, into=List[test_utils.NonConstructible])
