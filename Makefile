SHELL = /bin/bash -o pipefail
ALL_FILES = src test docs/conf.py

.PHONY: all
all: style lint coverage docs build


.PHONY: lint
lint: pylint mypy

.PHONY: pylint
pylint:
	poetry run pylint $(ALL_FILES)

.PHONY: mypy
mypy:
# Editors use mypy's incremental mode to provide error squiggles, reading config
# from setup.cfg. Certain options are not suitable for use in incremental mode,
# or produce output not suitable for error squiggles.
# Enable those options here instead of in setup.cfg.
	poetry run mypy \
	  --pretty \
	  --show-error-context \
	  --warn-redundant-casts \
	  --warn-unused-configs


.PHONY: style
style: flake8 isort_check black_check pydocstyle

.PHONY: autofmt
autofmt: isort black

.PHONY: flake8
flake8:
	poetry run flake8 $(ALL_FILES)

BLACK_COMMAND := poetry run black $(ALL_FILES)

.PHONY: black_check
black_check:
	$(BLACK_COMMAND) --check

.PHONY: black
black:
	$(BLACK_COMMAND)

ISORT_COMMAND := poetry run isort $(ALL_FILES)

.PHONY: isort_check
isort_check:
	$(ISORT_COMMAND) --check-only

.PHONY: isort
isort:
	$(ISORT_COMMAND)

.PHONY: pydocstyle
pydocstyle:
	poetry run pydocstyle $(ALL_FILES)


TOX_ENVS ?=

.PHONY: test
test:
ifdef TOX_ENVS
# Pin the pip version used by virtualenv - otherwise it uses the latest
# version.
	VIRTUALENV_PIP=21.2.3 poetry run tox -v -e $(TOX_ENVS)
	poetry run coverage combine .tox/*/tmp/.coverage
	poetry run coverage report
else
	poetry run coverage run -m pytest
endif

.PHONY: coverage
coverage: test
	poetry run coverage report --fail-under=100


.PHONY: docs
docs:
	rm -rf docs/_build
	poetry run sphinx-build -W -b html docs/ docs/_build


.PHONY: build
build:
	poetry build
