"""Tests for constructing class/function types from values."""

from typing import List, Tuple

import terramare
from terramare import VALUE, auto


def test_fn_optional_tail_fields_ok() -> None:
    """Optional tail fields should be ignored."""

    @auto(from_=VALUE)
    def fn(x: int, y: int = 0) -> Tuple[int, int]:
        return (x, y)

    assert terramare.structure(0, into=fn) == (0, 0)


def test_fn_skip_parameter_ok() -> None:
    @terramare.auto(from_=VALUE)
    @terramare.fields({"y": terramare.skip})
    def fn(x: int, y: int = 0) -> List[int]:
        return [x, y]

    assert terramare.structure(1, into=fn) == [1, 0]


def test_fn_skip_keyword_only_parameter_ok() -> None:
    @terramare.auto(from_=VALUE)
    @terramare.fields({"y": terramare.skip})
    def fn(
        x: int, *, y: terramare.Context = terramare.Context.EMPTY
    ) -> Tuple[int, terramare.Context]:
        return (x, y)

    assert terramare.structure(1, into=fn, context={"hello": "world"}) == (
        1,
        terramare.Context.EMPTY,
    )


def test_fn_from_context_required_ok() -> None:
    @terramare.auto(from_=VALUE)
    @terramare.fields({"y": terramare.metadata.from_context("int")})
    def fn(x: int, y: int) -> Tuple[int, int]:
        return (x, y)

    assert terramare.structure(0, into=fn, context={"int": 1}) == (0, 1)


def test_fn_from_context_optional_provided_ok() -> None:
    @terramare.auto(from_=VALUE)
    @terramare.fields({"y": terramare.metadata.from_context("int")})
    def fn(x: int, y: int = 0) -> Tuple[int, int]:
        return (x, y)

    assert terramare.structure(0, into=fn, context={"int": 1}) == (0, 1)


def test_fn_from_context_optional_value_missing_ok() -> None:
    @terramare.auto(from_=VALUE)
    @terramare.fields({"y": terramare.metadata.from_context("int")})
    def fn(x: int, y: int = 0) -> Tuple[int, int]:
        return (x, y)

    assert terramare.structure(0, into=fn, context={}) == (0, 0)


def test_fn_from_context_keyword_only_ok() -> None:
    @terramare.fields({"y": terramare.metadata.from_context("int")})
    @terramare.auto(from_=VALUE)
    def fn(x: int, *, y: int = 0) -> Tuple[int, int]:
        return (x, y)

    assert terramare.structure(0, into=fn, context={"int": 1}) == (0, 1)
