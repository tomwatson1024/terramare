"""Tests for constructing Literal types."""

from typing import Any, List

import pytest
from typing_extensions import Literal

from terramare import enums, terramare
from terramare.errors import ConstructorError, FactoryError, UnsupportedTargetTypeError
from terramare.types import Primitive

from . import test_utils
from .test_utils import raises


@pytest.mark.parametrize(
    ["literal_t", "input_value"],
    [
        pytest.param(Literal[None], None, id="literal None"),
        pytest.param(Literal[1], 1, id="literal int"),
        pytest.param(Literal["string"], "string", id="literal string"),
        pytest.param(Literal[1, 2], 1, id="multi-valued literal"),
        pytest.param(Literal[None, 1, "2"], 1, id="hetrogeneous literal"),
    ],
)
def test_construct_literal_mainline(
    literal_t: type,
    input_value: Primitive,
) -> None:
    """Test that a literal can be constructed."""
    assert (
        terramare.structure(input_value, into=literal_t) == input_value  # type: ignore
    )


@pytest.mark.parametrize(
    ["literal_t", "input_value", "expected_output_value"],
    [
        pytest.param(Literal[1], "1", 1, id="literal int"),
        pytest.param(Literal["string"], "string", "string", id="literal string"),
        pytest.param(Literal[1, 2], "1", 1, id="multi-valued literal"),
        pytest.param(Literal[None, 1, "2"], "1", 1, id="hetrogeneous literal"),
    ],
)
def test_construct_literal_coerce_strings_mainline(
    literal_t: type, input_value: Primitive, expected_output_value: Primitive
) -> None:
    """Test constructing a literal with coerce_strings enabled."""
    assert (
        terramare.structure(input_value, into=literal_t, coerce_strings=True)  # type: ignore
        == expected_output_value
    )


@pytest.mark.parametrize(
    ["literal_t", "input_value", "error_contains"],
    [
        pytest.param(Literal[1, 2], [], "expected int", id="type mismatch (list)"),
        pytest.param(
            Literal[1], [], "expected int", id="type mismatch (list), single variant"
        ),
        pytest.param(Literal[1, 2], "", "expected int", id="type mismatch (string)"),
        pytest.param(Literal[1, 2], 3, "expected one of {1,2}", id="value mismatch"),
        pytest.param(Literal[1], True, "expected int", id="literal int from bool"),
        pytest.param(Literal[1], 3, "expected 1", id="value mismatch, single variant"),
    ],
)
def test_construct_literal_error(
    literal_t: type, input_value: Primitive, error_contains: str
) -> None:
    """Test error cases when constructing a literal."""
    with raises(ConstructorError, assert_contains=error_contains):
        terramare.structure(input_value, into=literal_t)


@pytest.mark.parametrize(
    ["literal_t", "input_value", "error_contains"],
    [
        pytest.param(
            Literal[1, 2],
            [],
            "expected (string representation of) int",
            id="type mismatch (list)",
        ),
        pytest.param(
            Literal[1],
            [],
            "expected (string representation of) int",
            id="type mismatch (list), single variant",
        ),
        pytest.param(
            Literal[1, 2],
            "",
            "expected (string representation of) int",
            id="type mismatch (string)",
        ),
        pytest.param(Literal[1, 2], "3", "one of {1,2}", id="value mismatch"),
        pytest.param(
            Literal[1],
            "3",
            "expected (string representation of) 1",
            id="value mismatch, single variant",
        ),
    ],
)
def test_construct_literal_coerce_strings_error(
    literal_t: type, input_value: Primitive, error_contains: str
) -> None:
    """Test error cases when constructing a literal with coerce_strings enabled."""
    with raises(ConstructorError, assert_contains=error_contains):
        terramare.structure(input_value, into=literal_t, coerce_strings=True)


@pytest.mark.parametrize(
    ["non_literal_t"],
    [
        pytest.param(0),
        pytest.param(int),
        pytest.param(List),
        pytest.param(Any, id="Any"),
    ],
)
def test_not_a_literal_error(non_literal_t: Any) -> None:
    """Test creating a literal constructor from a non-literal."""
    with pytest.raises(UnsupportedTargetTypeError):
        test_utils.create_constructor_internal(enums.LiteralFactory(), non_literal_t)


def test_non_constructible_parameter_type() -> None:
    """Test a literal with a non-constructible parameter type."""
    with raises(FactoryError, assert_contains="only literal variants"):
        terramare.structure({}, into=Literal[3.14])
