"""Tests for terramare.pretty_printer module."""

from dataclasses import dataclass
from typing import (
    Any,
    Callable,
    Dict,
    List,
    Mapping,
    NewType,
    Optional,
    Sequence,
    Tuple,
    Union,
)

import pytest
from typing_extensions import Literal

from terramare import pretty_printer
from terramare.types import Primitive, Target


@pytest.mark.parametrize(
    ["value", "expected_output"],
    [
        pytest.param("s", '"s"', id="short string"),
        pytest.param(
            "a really really long string", '"a really real..."', id="long string"
        ),
        pytest.param(1, "1", id="int"),
        pytest.param(1.0, "1.0", id="float"),
        pytest.param({"a": 1, "b": 2}, '{"a": 1, "b": 2}', id="short dict"),
        pytest.param(
            {"a": 1, "b": 2, "c": 3, "d": 4}, '{"a": 1, "b": 2, ...}', id="long dict"
        ),
        pytest.param([1, 2], "[1, 2]", id="short list"),
        pytest.param([1, 2, 3, 4], "[1, 2, ...]", id="long list"),
        pytest.param(True, "True", id="bool"),
        pytest.param(None, "None", id="None"),
        pytest.param({"a": {"b": "c"}}, '{"a": {...}}', id="nested dict"),
        pytest.param([["a"]], "[[...]]", id="nested list"),
    ],
)
def test_print_primitive(value: Primitive, expected_output: str) -> None:
    """Test pretty-printing a primitive."""
    assert pretty_printer.print_primitive(value) == expected_output


@pytest.mark.parametrize(
    ["value", "expected_output"],
    [
        pytest.param(str, "str", id="str"),
        pytest.param(int, "int", id="int"),
        pytest.param(float, "float", id="float"),
        pytest.param(dict, "dict", id="dict"),
        pytest.param(list, "list", id="list"),
        pytest.param(bool, "bool", id="bool"),
        pytest.param(None, "None", id="None"),
        pytest.param(Callable, "Callable[..., Any]", id="Callable"),
        pytest.param(Callable[..., Any], "Callable[..., Any]", id="Callable[..., Any]"),
        pytest.param(
            Callable[..., bool], "Callable[..., bool]", id="Callable[..., bool]"
        ),
        pytest.param(Dict, "Dict[str, Any]", id="Dict[str, Any]"),
        pytest.param(Dict[str, Any], "Dict[str, Any]", id="Dict[str, Any]"),
        pytest.param(Dict[str, int], "Dict[str, int]", id="Dict[str, int]"),
        pytest.param(List, "List[Any]", id="List[Any]"),
        pytest.param(List[Any], "List[Any]", id="List[Any]"),
        pytest.param(List[int], "List[int]", id="List[int]"),
        pytest.param(NewType("T", int), "T", id="T"),
        pytest.param(Tuple, "Tuple[Any, ...]", id="Tuple[Any, ...]"),
        pytest.param(Tuple[int], "Tuple[int]", id="Tuple[int]"),
        pytest.param(Tuple[int, str], "Tuple[int, str]", id="Tuple[int, str]"),
        pytest.param(Tuple[int, ...], "Tuple[int, ...]", id="Tuple[int, ...]"),
        pytest.param(Optional[str], "Optional[str]", id="Optional[str]"),
        pytest.param(Union, "Union", id="Union"),
        pytest.param(Union[int], "int", id="Union[int]"),
        pytest.param(Union[int, str], "Union[int, str]", id="Union[int, str]"),
        pytest.param(Union[int, bool], "Union[int, bool]", id="Union[int, bool]"),
        pytest.param(Literal[1], "Literal[1]", id="Literal[1]"),
    ],
)
def test_print_type_name(value: Target[Any], expected_output: str) -> None:
    """Test pretty-printing a type name."""
    assert pretty_printer.print_type_name(value) == expected_output


def test_print_table() -> None:
    """Test pretty-printing a table."""
    rows: Sequence[Tuple[str, ...]] = [
        ("col1", "col2"),
        ("key", "one"),
        ("a really long key", "two"),
        ("shortkey", "a really long value"),
        ("novalue",),
    ]
    assert (
        pretty_printer.print_table(rows)
        == """
col1               col2
key                one
a really long key  two
shortkey           a really long value
novalue
""".strip()
    )


def test_print_empty_table() -> None:
    """Test pretty-printing an empty table."""
    assert pretty_printer.print_table([]) == ""


def test_print_object_mainline() -> None:
    """Test the recursive print_object function."""

    @dataclass(frozen=True)
    class TestClass:
        int_: int
        string: str
        tuple_: Tuple[int, int]
        sequence: Sequence[int]
        mapping: Mapping[str, int]

    assert (
        pretty_printer.print_object(
            TestClass(0, "hello", (1, 2), [3, 4, 5], {"six": 6})
        )
        == """
TestClass:
  int_:
    0
  string:
    hello
  tuple_(,):
  - 1
  - 2
  sequence[]:
  - 3
  - 4
  - 5
  mapping{}:
    six:
      6
""".strip()
    )


def test_print_object_simple() -> None:
    """Test printing an object with no members."""

    @dataclass(frozen=True)
    class TestClass:
        pass

    assert pretty_printer.print_object(TestClass()) == "TestClass"
