"""Tests for terramare.terramare module."""

import configparser
import json
from typing import Any, Callable, Dict, List, Optional, Tuple, Union, cast

import attr
import pytest
import ruamel.yaml
import toml

import terramare
from terramare.errors import ConstructorError
from terramare.types import Primitive

from .test_utils import raises


@terramare.disallow_unknown_fields
@attr.s(auto_attribs=True, frozen=True)
class _SimpleClass:
    name: str
    value: int
    flag: bool


@attr.s(auto_attribs=True, frozen=True)
class _NestedClass:
    first_section: _SimpleClass
    second_section: _SimpleClass


def _configparser_loads(content: str) -> Dict[str, Dict[str, str]]:
    parser = configparser.ConfigParser()
    parser.read_string(content)
    return {section: dict(parser[section]) for section in parser.sections()}


@pytest.mark.parametrize(
    ["load_fn", "content", "kwargs"],
    [
        pytest.param(
            json.loads,
            """
{
  "first_section": {"name": "one", "value": 1, "flag": true},
  "second_section": {"name": "two", "value": 2, "flag": false}
}
            """,
            {},
            id="json",
        ),
        pytest.param(
            lambda s: ruamel.yaml.YAML(typ="safe", pure=True).load(s),
            """
first_section:
  name: one
  value: 1
  flag: true
second_section:
  name: two
  value: 2
  flag: false
            """,
            {},
            id="yaml",
        ),
        pytest.param(
            toml.loads,
            """
[first_section]
name = "one"
value = 1
flag = true

[second_section]
name = "two"
value = 2
flag = false
            """,
            {},
            id="toml",
        ),
        pytest.param(
            _configparser_loads,
            """
[first_section]
name: one
value: 1
flag: True

[second_section]
name: two
value: 2
flag: false
        """,
            {"coerce_strings": True},
            id="configparser",
        ),
    ],
)
def test_nested_class(
    load_fn: Callable[[str], Primitive], content: str, kwargs: Any
) -> None:
    """Construct a nested class from various serialized data types."""
    terramare.structure(load_fn(content), into=_NestedClass, **kwargs)


@terramare.auto
class _ComplexClass:
    def __init__(  # type: ignore[no-untyped-def]
        self,
        list_arg: List[_SimpleClass],
        tuple_arg: Tuple[str, str],
        union_arg_1: Union[str, int],
        union_arg_2: Union[str, int],
        untyped_arg,
        optional_kwarg_1: Optional[Dict[str, int]] = None,
        optional_kwarg_2: Optional[Dict[str, int]] = None,
        untyped_kwarg=None,
    ) -> None:
        self.list_arg = list_arg
        self.tuple_arg = tuple_arg
        self.union_arg_1 = union_arg_1
        self.union_arg_2 = union_arg_2
        self.untyped_arg = untyped_arg
        self.optional_kwarg_1 = optional_kwarg_1 if optional_kwarg_1 is not None else {}
        self.optional_kwarg_2 = optional_kwarg_2 if optional_kwarg_2 is not None else {}
        self.untyped_kwarg = untyped_kwarg
        self.something_else = "something else"

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, type(self)):
            return NotImplemented
        return vars(self) == vars(other)

    def __repr__(self) -> str:
        return repr(vars(self))


@pytest.mark.parametrize(
    ["load_fn", "content"],
    [
        pytest.param(
            json.loads,
            """
{
  "list_arg": [
    {"name": "one", "value": 1, "flag": true}
  ],
  "tuple_arg": ["two", "2"],
  "union_arg_1": 3,
  "union_arg_2": "three",
  "untyped_arg": {"four": 4},
  "optional_kwarg_1": {"five": 5},
  "optional_kwarg_2": null,
  "untyped_kwarg": [3, 4, 5]
}
            """,
            id="json",
        ),
        pytest.param(
            lambda s: ruamel.yaml.YAML(typ="safe", pure=True).load(s),
            """
list_arg:
- name: one
  value: 1
  flag: true
tuple_arg: ["two", "2"]
union_arg_1: 3
union_arg_2: "three"
untyped_arg:
  four: 4
optional_kwarg_1:
  five: 5
optional_kwarg_2: null
untyped_kwarg:
- 3
- 4
- 5
            """,
            id="yaml",
        ),
        pytest.param(
            toml.loads,
            """
union_arg_1 = 3
union_arg_2 = "three"
tuple_arg = ["two", "2"]
untyped_kwarg = [3, 4, 5]

[[list_arg]]
name = "one"
value = 1
flag = true

[untyped_arg]
four = 4

[optional_kwarg_1]
five = 5
            """,
            id="toml",
        ),
    ],
    ids=["json", "yaml", "toml"],
)
def test_complex_class(load_fn: Callable[[str], Primitive], content: str) -> None:
    """Construct a complex class from various serialized data types."""
    assert terramare.structure(load_fn(content), into=_ComplexClass) == _ComplexClass(
        [_SimpleClass("one", 1, True)],
        ("two", "2"),
        3,
        "three",
        {"four": 4},
        {"five": 5},
        None,
        [3, 4, 5],
    )


@pytest.mark.parametrize(
    ["target_t", "primitive"],
    [
        pytest.param(_SimpleClass, {}, id="missing elements"),
        pytest.param(_SimpleClass, ["", 1, False, 2], id="extra elements"),
        pytest.param(_SimpleClass, {}, id="missing fields"),
        pytest.param(
            _SimpleClass,
            {"name": "", "value": 1, "flag": False, "extra": 2},
            id="extra fields",
        ),
        pytest.param(
            _SimpleClass,
            {"name": 0, "value": "", "flag": ""},
            id="multiple type mismatches",
        ),
        pytest.param(
            _NestedClass,
            {
                "first_section": {"name": 0, "value": False, "flag": ""},
                "second_section": [],
            },
            id="nested",
        ),
        pytest.param(
            _ComplexClass,
            {
                "list_arg": [
                    {"name": "", "value": 1, "flag": False},
                    {"name": ""},
                    {"name": 0, "value": "", "flag": ""},
                    {"name": "", "value": 1, "flag": False, "extra": 2},
                ],
                "tuple_arg": ["", 0],
                "union_arg_1": True,
                "union_arg_2": "",
                "untyped_arg": ["a", "list"],
            },
            id="complex class",
        ),
    ],
)
def test_construction_error(target_t: Any, primitive: Primitive) -> None:
    """
    Attempt to construct an object from a mismatched primitive.

    This isn't an interesting _automated_ test, but it's useful for checking the
    readability of terramare's errors.
    """
    with raises(ConstructorError):
        terramare.structure(primitive, into=target_t)


def test_constructor_non_primitive() -> None:
    """Attempt to call a constructor with non-primitive data."""
    with pytest.raises(TypeError):
        terramare.structure(cast(Primitive, lambda: None), into=int)


def test_constructor_non_bool_coerce_strings() -> None:
    """Attempt to call a constructor with a non-bool coerce_strings value."""
    with pytest.raises(TypeError):
        terramare.structure({}, into=int, coerce_strings=cast(bool, []))
