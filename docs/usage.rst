Usage
=====

Installation
------------

Using pip
^^^^^^^^^

Run::

   pip install terramare

Using Poetry
^^^^^^^^^^^^

Run::

   poetry add terramare

Introduction
------------

``terramare`` exposes a single function, :py:func:`terramare.structure`.
The function takes a type and a *primitive value* - that is, a value
that is either:

- A ``string``;
- An ``int``;
- A ``float``;
- A ``bool``;
- ``None``;
- A ``list``, containing only primitive values;
- A ``dict``, mapping primitive keys to primitive values.

Thus ``1``, ``[1, "a"]``, and ``{"key": ["a", "b"]}`` are all primitive values.

The function will attempt to construct an instance the type from the supplied
primitive, raising a descriptive error if this fails.

``terramare`` uses Python's standard type hints to determine *how* to construct
this instance.

Basic Usage
-----------

``terramare`` aims to "just work" right out of the box for simple use-cases - provided that type hints are present!

.. warning::
    ``terramare`` *requires* type hints to function correctly!

The following families of types are supported:

- Primitive types, as above;
- A handful of standard library types: paths, datetimes, regexes, and IP address;
- Built-in generic types such as ``Dict``, ``List``, or ``Optional``, parametrized over any supported type;
- Unions of supported types;
- ``NamedTuple``-based classes, which will be deserialized from lists or dictionaries;
- Dataclasses and `attrs <https://github.com/python-attrs/attrs>`_ classes, which will be deserialized from dictionaries;
- Any class or function for which metadata is supplied - see below.

:func:`terramare.structure` may throw three types of exceptions:

- :exc:`terramare.ConstructorError` if the supplied data cannot be used to construct an instance of the supplied type.
  This is the error thrown if, for example, a user has supplied invalid config to your application.
- :exc:`terramare.FactoryError` if a deserializer cannot be created for the supplied type.
  Such errors are likely to require changes to the type or the invocation of ``terramare``.

.. note::
    :exc:`terramare.ConstructorError` s should likely be caught and reported to the user.
    It is unlikely to be worth catching instances of :exc:`terramare.FactoryError`.

Usage with ConfigParser
-----------------------

Python's ``configparser`` module does not immediately work with ``terramare``:

- The module reads text into a custom type, rather than a primitive value;
- All values are read as strings.

However, these problems can easily be mitigated. A ``ConfigParser`` object
``parser`` can be converted to a ``dict``::

   {section: dict(parser[section]) for section in parser.sections()}

and ``terramare`` can coerce strings into the correct types, by passing
``coerce_strings=True`` to ``deserialize_into``.

Example
^^^^^^^

>>> import attr
>>> import configparser
>>> import terramare
>>>
>>> config = """
... [user]
... id = 1
... name = Alice
... """
>>>
>>> @attr.s(auto_attribs=True)
... class User:
...     id: int
...     name: str
>>>
>>> @attr.s(auto_attribs=True)
... class Config:
...     user: User
>>>
>>> parser = configparser.ConfigParser()
>>> parser.read_string(config)
>>> print(terramare.structure(
...     {section: dict(parser[section]) for section in parser.sections()},
...     into=Config,
...     coerce_strings=True,
... ))
Config(user=User(id=1, name='Alice'))

Validation
----------

``terramare`` does not itself provide validation of the parsed data beyond its type.
However, since ``terramare`` calls the user-provided ``__init__`` methods, custom validation or validation provided by another library is simple to integrate with ``terramare``.

A class with validation should be decorated with :class:`terramare.handle_exception_types`, specifying the exception types raised if validation fails.

See the documentation for :class:`terramare.handle_exception_types` for more details.

Example
^^^^^^^

Using `attrs <https://github.com/python-attrs/attrs>`_ to provide validation:

>>> import json
>>> import attr
>>> import terramare
>>>
>>> @terramare.handle_exception_types(ValueError)
... @attr.s(auto_attribs=True)
... class User:
...     id: int = attr.ib()
...     name: str
...
...     @id.validator
...     def positive(self, _, value):
...         if not value > 0:
...             raise ValueError("id must be positive!")
>>>
>>> terramare.structure(
...     json.loads('{"id": 1, "name": "Alice"}'),
...     into=User,
... )
User(id=1, name='Alice')
>>>
>>> terramare.structure(
...     json.loads('{"id": 0, "name": "Bob"}'),
...     into=User,
... )
Traceback (most recent call last):
...
terramare.errors.ConstructorError: .: id must be positive!
...

Using a class with hand-written validation in the ``__init__`` method:

>>> import json
>>> import terramare
>>>
>>> @terramare.auto
... @terramare.handle_exception_types(ValueError)
... class User:
...     def __init__(self, id: int, name: str) -> None:
...         if not id > 0:
...             raise ValueError("id must be positive!")
...         self.id = id
...         self.name = name
...
...     def __repr__(self) -> str:
...         return f"User(id={self.id}, name='{self.name}')"
>>>
>>> terramare.structure(
...     json.loads('{"id": 1, "name": "Alice"}'),
...     into=User,
... )
User(id=1, name='Alice')
>>>
>>> terramare.structure(
...     json.loads('{"id": 0, "name": "Bob"}'),
...     into=User,
... )
Traceback (most recent call last):
...
terramare.errors.ConstructorError: .: id must be positive!
...
