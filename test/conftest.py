"""Pytest setup."""

import sys

import pytest

_PYTHON_VERSION_MARKS = {
    "py39plus": lambda m: m >= 9,
    "py310plus": lambda m: m >= 10,
}


def pytest_runtest_setup(item: pytest.Item) -> None:
    """Run for each test."""
    python_version_marks = [
        _PYTHON_VERSION_MARKS[mark.name]
        for mark in item.iter_markers()
        if mark.name in _PYTHON_VERSION_MARKS
    ]
    if python_version_marks and not any(
        mark(sys.version_info.minor) for mark in python_version_marks
    ):
        pytest.skip(
            f"test disabled on Python {sys.version_info.major}.{sys.version_info.minor}"
        )
